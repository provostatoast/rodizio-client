/* eslint no-trailing-spaces:0 */
/* eslint no-plusplus:0 */

import hPans from '../data/hotelPans';
import panDetails from '../data/panDetails';
import savedSetups from '../data/savedSetups';
import {
  sortObject,
  allPossibleCases,
  configFromDups,
  countDups,
  removeAllItemsFromArray,
  getDupsFromObject,
} from '../utility';

const pool = [
  { pan: 'twoThird', depth: 6 },
  { pan: 'twoThird', depth: 6 },
  { pan: 'full', depth: 2 },
  { pan: 'oneSixth', depth: 2 },
  { pan: 'oneSixth', depth: 2 },
  { pan: 'oneNinth', depth: 4 },
  { pan: 'oneNinth', depth: 4 },
  { pan: 'oneNinth', depth: 4 },
  { pan: 'twoThird', depth: 4 },
];
let matchingConfigs = pool;
const completedBayConfigurations = {};

const fullConfigDups = panDetails.full.equivalents;
let fullConfigs = {};
let leftovers = {};

function configInPool(configDupsObj, poolDupsObj) {
  let configValid = true;
  let validConfigObject = false;

  Object.keys(configDupsObj).every((key) => {
    if (configDupsObj[key] <= poolDupsObj[key]) {
      return true;
    }
    configValid = false;
    return false;
  });

  if (configValid) {
    validConfigObject = configFromDups(configDupsObj);
  }

  return validConfigObject;
}

function dimensionsFit(pan, initialW, initialL) {
  const area = initialW * initialL;
  const panArea = pan.width * pan.length;
  const lDiff = initialL - pan.length;
  const wDiff = initialW - pan.width;

  // pan area doesn't work
  if (panArea > area) {
    return false;
  }

  // first orientation
  if (lDiff >= 0 && wDiff >= 0) {
    return true;
  }
  // TODO: second orientation

  // Areas work together, but dimensions don't
  return false;
}

export const largestConfigForArea = (w, l) => {
  let count = 0;
  const origWidth = w;
  const panConfigsForArea = {};

  function panRecur(pan, width, length, k) {
    panConfigsForArea[`config${k}`] = panConfigsForArea[`config${k}`] || [];
    let wi = width;
    let li = length;
    let possibleFits;

    if (dimensionsFit(pan, wi, li)) {
      panConfigsForArea[`config${k}`].push(pan);
      wi -= pan.width;
      if (wi < 108) {
        wi = origWidth;
        li -= pan.length;
      }
      if (dimensionsFit(pan, wi, li)) {
        panRecur(pan, wi, li, k);
      } else {
        possibleFits = hPans.filter(p => dimensionsFit(p, wi, li));
        if (possibleFits.length > 0) {
          panRecur(possibleFits[0], wi, li, k);
        }
      }
    }
  }

  hPans.forEach((hp) => {
    count++;
    return panRecur(hp, w, l, count);
  });

  return panConfigsForArea;
};

function sortByDepth() {
  const pansByDepths = {};

  pool.forEach((p) => {
    const depth = `${p.depth}d`;
    return pansByDepths[depth]
    ? pansByDepths[depth].push(p.pan)
    : pansByDepths[depth] = [p.pan];
  });

  return pansByDepths;
}

// function flattenConfigurations(configs) {
//   const flattened = [];
//
//   Object.keys(configs).forEach((key) => {
//     const configArray = configs[key].map(c => c.name);
//
//     if (configArray.length > 0) flattened.push(configArray);
//   });
//
//   return flattened;
// }

// function removeAllItemsFromArray(arr1, arr2) {
//   const result = arr2;
//   for (let i = 0; i < arr1.length; i++) {
//     if (arr2.indexOf(arr1[i]) > -1) {
//       result.splice(arr2.indexOf(arr1[i]), 1);
//     }
//   }
//
//   return result;
// }

// function getDupsFromObject(obj) {
//   const dupObj = {};
//
//   Object.keys(obj).forEach((k) => {
//     const configDups = countDups(obj[k]);
//     dupObj[k] = configDups;
//   });
//
//   return dupObj;
// }

function getConfigurations(availablePans, fConfigDups, cb) {
  const pansByDepth = (availablePans.constructor === Array)
    ? sortByDepth(availablePans)
    : availablePans;
  const lo = {};
  const mc = {};

  Object.keys(pansByDepth).forEach((k) => {
    const level = countDups(pansByDepth[k]);
    mc[k] = [];
    fConfigDups.forEach((config) => {
      const validConfig = level ? configInPool(config, level) : false;
      if (validConfig) {
        mc[k].push(validConfig);
        pansByDepth[k] = removeAllItemsFromArray(validConfig, pansByDepth[k]);
      }
    });
    lo[k] = pansByDepth[k];
  });

  cb(mc, lo);
}

function handleLeftovers() {
  const origLeftovers = leftovers;
  leftovers = {};

  Object.keys(origLeftovers).forEach((k) => {
    const level = countDups(origLeftovers[k]);
    fullConfigs[k] = [];
    fullConfigDups.forEach((config) => {
      const validConfig = configInPool(config, level);
      if (validConfig) {
        fullConfigs[k].push(validConfig);
        origLeftovers[k] = removeAllItemsFromArray(validConfig, origLeftovers[k]);
      }
    });
    if (Object.keys(origLeftovers[k]).length > 0) {
      leftovers[k] = origLeftovers[k];
    }
  });
}

function setMatchingConfigsAndLeftovers(mc, lo) {
  leftovers = sortObject(lo);
  matchingConfigs = sortObject(mc);

  getConfigurations(leftovers, fullConfigDups, (m, l) => {
    fullConfigs = sortObject(m);
    leftovers = sortObject(l);
  });

  console.log('MATCHING ENTIRE CONFIGS VVVVVVVV');
  console.log(matchingConfigs);
  console.log('********************************************');
  console.log('MATCHING FULL CONFIGS VVVVVVVV');
  console.log(fullConfigs);
  console.log('********************************************');
  console.log('LEFTOVERS VVVVVVVV');
  console.log(leftovers);
}

function panConfigurations() {
  Object.keys(savedSetups.roadcase).forEach((key) => {
    console.log(`======= START OF ${key} ========`);
    if (savedSetups.roadcase.hasOwnProperty(key)) {
      let panDepth = savedSetups.roadcase[key].depth;
      // const conf = savedSetups.roadcase[key].largestConfig();
      // const flatConf = flattenConfigurations(conf);
      const confDups = allPossibleCases([
        panDetails.full.equivalents,
        panDetails.oneThird.equivalents,
      ]);
      // confDups.push({ twoThird: 2 });
      const completedBay = {};

      getConfigurations(matchingConfigs, confDups, setMatchingConfigsAndLeftovers);

      Object.keys(matchingConfigs).forEach((mc) => {
        let i = 0;
        const matchingConfigDepth = Number(mc.replace('d', ''));
        if (matchingConfigs[mc].length > 0) {
          completedBay[mc] = [];
          while (panDepth - matchingConfigDepth >= 0) {
            if (matchingConfigs[mc][i] !== undefined) {
              completedBay[mc].push(matchingConfigs[mc][i]);
            }
            panDepth -= matchingConfigDepth;
            i++;
          }
          completedBayConfigurations[key] = completedBay;
          return;
        }
        return;
      });

      /*
        get the bay height. begin at the largest depth and add the array val to the bay recommendation
        subtract config's height from the bay height. look for the biggest key that is equal to or less than the remaining height of the bay, add the first item in the depth array
        repeat until there are no more options in entire configs, look for the same pattern in full configs, then try with the leftovers

        when bay is full, remove the pans that are configured in the bay from the pool of options, reset the pool of options to this new array, then move to the next bay.
      */

      // console.log(completedBayConfigurations[key]);

      console.log(`======= END OF ${key} ========`);
      console.log('');
      console.log('');
      // console.log('=========================');
      // console.log('=========================');
      // console.log('=========================');
      // console.log('=========================');
    }
  });
}

export default panConfigurations;
// console.log(largestConfigForArea(324, 704))
