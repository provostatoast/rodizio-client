import api from '../api';
import { setIngredientsToState } from '../actions/ingredients';
import { setRangeValues, setPercentages } from '../actions/ranges';
import { setName } from '../actions/name';
import { titleCase } from '../utility';

export function recipesHasErrored(bool) {
  return {
    type: 'RECIPES_FETCHING_ERROR',
    hasErrored: bool,
  };
}

export function recipesIsLoading(bool) {
  return {
    type: 'RECIPES_LOADING',
    recipesLoading: bool,
  };
}

export function recipeIsSaving(bool) {
  return {
    type: 'RECIPE_SAVING',
    recipeSaving: bool,
  };
}

export function saveRecipeSuccess(recipe) {
  return {
    type: 'SAVE_RECIPE_SUCCESS',
    recipe,
  };
}

export function setRecipeMessage(recipeMessage) {
  return {
    type: 'RECIPE_MESSAGE',
    recipeMessage,
  };
}

export function recipesFetchSuccess(recipes) {
  return {
    type: 'RECIPES_FETCH_SUCCESS',
    recipes,
  };
}

export function singleRecipeFetchSuccess(recipe) {
  return {
    type: 'SINGLE_RECIPE_FETCH_SUCCESS',
    recipe,
  };
}

export function fetchAllRecipes() {
  return (dispatch) => {
    dispatch(recipesIsLoading(true));

    api.getAllRecipes()
      .then((recipes) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(recipesIsLoading(false));
        return recipes.data;
      })
      .then(recipes => (
        dispatch(recipesFetchSuccess(recipes))
      ))
      .catch(() => dispatch(recipesHasErrored(true)));
  };
}

export function fetchRecipeById(id) {
  return (dispatch) => {
    dispatch(recipesIsLoading(true));

    api.getRecipeById(id)
      .then((recipe) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(recipesIsLoading(false));
        return recipe.data;
      })
      .then((recipe) => {
        dispatch(singleRecipeFetchSuccess(recipe));
        return recipe;
      })
      .then((recipe) => {
        const {
          rangeValues,
          ingredientsPercentages,
          ingredients,
          name,
        } = recipe;
        dispatch(setName(name));
        dispatch(setRangeValues(rangeValues));
        dispatch(setPercentages(ingredientsPercentages));
        dispatch(setIngredientsToState(ingredients));
        return recipe;
      })
      .catch(() => dispatch(recipesHasErrored(true)));
  };
}

export function addNewRecipe() {
  return (dispatch, getState) => {
    dispatch(recipeIsSaving(true));
    const {
      ingredients,
      name,
      rangeValues,
      ingredientsPercentages,
      location,
    } = getState();
    const recipeObj = {};
    const newIngredients = ingredients.map((ing) => {
      const ingObj = { ingredient: ing.value };
      return ingObj;
    });

    recipeObj.name = titleCase(name);
    recipeObj.ingredients = newIngredients;
    recipeObj.rangeValues = rangeValues;
    recipeObj.ingredientsPercentages = ingredientsPercentages;

    api.addNewRecipe(recipeObj)
      .then(() => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(recipeIsSaving(false));
        const { from } = location.state || {
          from: { pathname: '/recipes' },
        };
        getState().setIngredients([]);
        getState().history.push(from.pathname);
      })
      // .then(recipe => (
      //   dispatch(saveRecipeSuccess(recipe))
      // ))
      .catch(() => dispatch(recipesHasErrored(true)));
  };
}

export function updateRecipe(payload) {
  return (dispatch) => {
    dispatch(recipeIsSaving(true));

    api.updateRecipe(payload)
      .then((recipe) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(recipeIsSaving(false));
        return recipe.data;
      })
      .then(recipe => (
        dispatch(saveRecipeSuccess(recipe))
      ))
      .catch(() => dispatch(recipesHasErrored(true)));
  };
}

export function deleteRecipe(id) {
  return (dispatch, getState) => {
    const recipes = getState()
      .recipes.filter(ing => ing._id !== id);

    dispatch(recipeIsSaving(true));

    api.deleteRecipe(id)
      .then((success) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(recipeIsSaving(false));
        return success.status;
      })
      .then(message => (
        dispatch(setRecipeMessage(message))
      ))
      .then(() => {
        dispatch(recipesFetchSuccess(recipes));
      })
      .catch(() => dispatch(recipesHasErrored(true)));
  };
}
