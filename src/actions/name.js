export const setName = name => (dispatch) => {
  dispatch({
    type: 'SET_NAME',
    payload: name,
  });
};
