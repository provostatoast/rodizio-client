import api from '../api';
import { setRangeValueArray } from '../actions/ranges';
import { getRandomColor } from '../utility';

export function ingredientsHasErrored(bool) {
  return {
    type: 'INGREDIENTS_FETCHING_ERROR',
    hasErrored: bool,
  };
}

export function ingredientsIsLoading(bool) {
  return {
    type: 'INGREDIENTS_LOADING',
    ingredientsLoading: bool,
  };
}

export function ingredientIsSaving(bool) {
  return {
    type: 'INGREDIENT_SAVING',
    ingredientSaving: bool,
  };
}

export function saveIngredientSuccess(ingredient) {
  return {
    type: 'SAVE_INGREDIENT_SUCCESS',
    ingredient,
  };
}

export function setIngredientMessage(ingredientMessage) {
  return {
    type: 'INGREDIENT_MESSAGE',
    ingredientMessage,
  };
}

export function ingredientsFetchSuccess(ingredients) {
  return {
    type: 'INGREDIENTS_FETCH_SUCCESS',
    ingredients,
  };
}

export function singleIngredientFetchSuccess(ingredient) {
  return {
    type: 'SINGLE_INGREDIENT_FETCH_SUCCESS',
    ingredient,
  };
}


export function setIngredientsToState(ingredients) {
  return {
    type: 'SET_INGREDIENTS',
    ingredients: ingredients.map((ing) => {
      const ingCopy = ing;
      ingCopy.color = getRandomColor();
      return ingCopy;
    }),
  };
}

export function processIngredients(ingredients) {
  return (dispatch) => {
    dispatch(setRangeValueArray(ingredients));
    dispatch(setIngredientsToState(ingredients));
  };
}

export function fetchAllIngredients() {
  return (dispatch) => {
    dispatch(ingredientsIsLoading(true));

    api.getAllIngredients()
      .then((ingredients) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(ingredientsIsLoading(false));
        return ingredients.data;
      })
      .then(ingredients => (
        dispatch(ingredientsFetchSuccess(ingredients))
      ))
      .catch(() => dispatch(ingredientsHasErrored(true)));
  };
}

export function fetchIngredientById(id) {
  return (dispatch) => {
    dispatch(ingredientsIsLoading(true));

    api.getIngredientById(id)
      .then((ingredient) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(ingredientsIsLoading(false));
        return ingredient.data;
      })
      .then(ingredient => (
        dispatch(singleIngredientFetchSuccess(ingredient))
      ))
      .catch(() => dispatch(ingredientsHasErrored(true)));
  };
}

export function addNewIngredient(payload) {
  return (dispatch) => {
    dispatch(ingredientIsSaving(true));

    api.addNewIngredient(payload)
      .then(() => {
        dispatch(ingredientIsSaving(false));
        return dispatch(fetchAllIngredients());
      })
      .catch(() => dispatch(ingredientsHasErrored(true)));
  };
}

export function updateIngredient(payload) {
  return (dispatch) => {
    dispatch(ingredientIsSaving(true));

    api.updateIngredient(payload)
      .then((ingredient) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(ingredientIsSaving(false));
        return ingredient.data;
      })
      .then(ingredient => (
        dispatch(saveIngredientSuccess(ingredient))
      ))
      .catch(() => dispatch(ingredientsHasErrored(true)));
  };
}

export function deleteIngredient(id) {
  return (dispatch, getState) => {
    const ingredients = getState()
      .ingredients.filter(ing => ing._id !== id);

    dispatch(ingredientIsSaving(true));

    api.deleteIngredient(id)
      .then((success) => {
        // if (err) {
        //   throw Error(err);
        // }
        dispatch(ingredientIsSaving(false));
        return success.status;
      })
      .then(message => (
        dispatch(setIngredientMessage(message))
      ))
      .then(() => {
        dispatch(ingredientsFetchSuccess(ingredients));
      })
      .catch(() => dispatch(ingredientsHasErrored(true)));
  };
}
