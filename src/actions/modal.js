export function showModal() {
  return {
    type: 'SHOW',
  };
}

export function hideModal() {
  return {
    type: 'HIDE',
  };
}
