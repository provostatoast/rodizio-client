// import { setRangeValueArray } from '../utility';
const calculateIngredientsPercentages = (rangeValues) => {
  const ingredientsLength = rangeValues.length;
  const ingredientsPercentages = [];

  for (let i = 1; i < ingredientsLength; i += 1) {
    const value = rangeValues[i] - rangeValues[i - 1];
    ingredientsPercentages.push(value);
  }

  return ingredientsPercentages;
};

export const setRangeValues = rangeValues => (dispatch) => {
  dispatch({
    type: 'SET_RANGE_VALUES',
    payload: rangeValues,
  });
};

export const setPercentages = percentages => (dispatch) => {
  dispatch({
    type: 'SET_PERCENTAGES',
    payload: percentages,
  });
};

export const setRangeValueArray = ingredients => (dispatch) => {
  const ingredientsLength = ingredients.length;
  const ingredientsDiv = Math.floor(100 / ingredientsLength);
  const rangeValues = [0];
  for (let i = 1; i < ingredientsLength; i += 1) {
    rangeValues[i] = ingredientsDiv * i;
  }

  rangeValues[ingredientsLength] = 100;

  const ingredientsPercentages = calculateIngredientsPercentages(rangeValues);

  dispatch(setRangeValues(rangeValues));
  dispatch(setPercentages(ingredientsPercentages));
};
