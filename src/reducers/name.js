import { SET_NAME } from './constants';

export default function nameReducer(state = '', action) {
  switch (action.type) {
    case SET_NAME:
      return action.payload;
    default:
      return state;
  }
}
