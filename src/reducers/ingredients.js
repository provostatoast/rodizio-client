export default function ingredientsReducer(state = [], action) {
  switch (action.type) {
    case 'INGREDIENTS_FETCH_SUCCESS':
      return action.ingredients;
    case 'SAVE_INGREDIENT_SUCCESS':
      return action.ingredient;
    case 'SET_INGREDIENTS':
      return action.ingredients;
    default:
      return state;
  }
}

export const setIngredients = ingredients => (dispatch) => {
  dispatch({
    type: 'SET_INGREDIENTS',
    ingredients,
  });
  return Promise.resolve();
};

export function nameReducer(state = [], action) {
  switch (action.type) {
    case 'SET_INGREDIENTS':
      return action.ingredients;
    case 'SAVE_INGREDIENT_SUCCESS':
      return action.ingredient;
    default:
      return state;
  }
}
