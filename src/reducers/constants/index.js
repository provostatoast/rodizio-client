/* eslint no-unused-vars:0 */

// auth
export const AUTH_USER = 'AUTH_USER';
export const UNAUTH_USER = 'UNAUTH_USER';
export const REGISTER_FAIL = 'REGISTER_FAIL';
export const LOGIN_FAIL = 'LOGIN_FAIL';

// quantity
export const SET_PEOPLE = 'SET_PEOPLE';
export const SET_OZ = 'SET_OZ';

// ranges
export const SET_RANGE_VALUES = 'SET_RANGE_VALUES';
export const SET_PERCENTAGES = 'SET_PERCENTAGES';

// name
export const SET_NAME = 'SET_NAME';

// ingredients
export const SET_INGREDIENTS = 'SET_INGREDIENTS';

// messages
export const ITEM_SAVED = 'ITEM_SAVED';
export const ERROR = 'ERROR';
