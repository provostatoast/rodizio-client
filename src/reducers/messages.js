import { ITEM_SAVED, ERROR } from './constants';

export const addSavedItemMessage = message => (dispatch) => {
  dispatch({
    type: ITEM_SAVED,
    payload: message,
  });
};

export const addErrorMessage = message => (dispatch) => {
  dispatch({
    type: ERROR,
    payload: message,
  });
};

export default function messageReducer(state = {}, action) {
  switch (action.type) {
    case ITEM_SAVED:
      return action.payload;
    case ERROR:
      return action.payload;
    default:
      return state;
  }
}
