import { SET_OZ } from './constants';

export const setOzPerPerson = amount => (dispatch) => {
  dispatch({
    type: SET_OZ,
    payload: amount,
  });
  return Promise.resolve();
};

export default function quantityReducer(state = 16, action) {
  switch (action.type) {
    case SET_OZ:
      return action.payload;
    default:
      return state;
  }
}
