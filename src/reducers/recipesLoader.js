export default function recipesLoaderReducer(state = false, action) {
  switch (action.type) {
    case 'RECIPES_LOADING':
      return action.recipesLoading;
    default:
      return state;
  }
}
