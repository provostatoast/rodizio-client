import { SET_PERCENTAGES } from './constants';

export const setPercentages = percentageValues => (dispatch) => {
  dispatch({
    type: SET_PERCENTAGES,
    payload: percentageValues,
  });
  return Promise.resolve();
};

export default function percentageReducer(state = [100], action) {
  switch (action.type) {
    case SET_PERCENTAGES:
      return action.payload;
    default:
      return state;
  }
}
