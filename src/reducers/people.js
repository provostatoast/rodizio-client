import { SET_PEOPLE } from './constants';

export const setPeople = people => (dispatch) => {
  dispatch({
    type: SET_PEOPLE,
    payload: people,
  });
};

export default function peopleReducer(state = 25, action) {
  switch (action.type) {
    case SET_PEOPLE:
      return action.payload;
    default:
      return state;
  }
}
