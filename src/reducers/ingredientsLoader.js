export default function ingredientsLoaderReducer(state = false, action) {
  switch (action.type) {
    case 'INGREDIENTS_LOADING':
      return action.ingredientsLoading;
    default:
      return state;
  }
}
