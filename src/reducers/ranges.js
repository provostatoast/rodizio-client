import { SET_RANGE_VALUES } from './constants';

export const setRangeValues = rangeValues => (dispatch) => {
  dispatch({
    type: SET_RANGE_VALUES,
    payload: rangeValues,
  });
  return Promise.resolve();
};

export default function rangeReducer(state = [0, 0], action) {
  switch (action.type) {
    case SET_RANGE_VALUES:
      return action.payload;
    default:
      return state;
  }
}
