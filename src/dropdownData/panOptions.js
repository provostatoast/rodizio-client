const panOptions = [
  {
    text: 'Full Pan, 1" Deep',
    value: 'full-1',
  },
  {
    text: 'Full Pan, 2" Deep',
    value: 'full-2',
  },
  {
    text: 'Full Pan, 4" Deep',
    value: 'full-4',
  },
  {
    text: 'Full Pan, 6" Deep',
    value: 'full-6',
  },
  {
    text: 'Full Pan, 8" Deep',
    value: 'full-8',
  },
  {
    text: '1/2 Pan, 1" Deep',
    value: 'half-1',
  },
  {
    text: '1/2 Pan, 2" Deep',
    value: 'half-2',
  },
  {
    text: '1/2 Pan, 4" Deep',
    value: 'half-4',
  },
  {
    text: '1/2 Pan, 6" Deep',
    value: 'half-6',
  },
  {
    text: '1/2 Pan, 8" Deep',
    value: 'half-8',
  },
  {
    text: '1/2 (Long) Pan, 1" Deep',
    value: 'halfLong-1',
  },
  {
    text: '1/2 (Long) Pan, 2" Deep',
    value: 'halfLong-2',
  },
  {
    text: '1/2 (Long) Pan, 4" Deep',
    value: 'halfLong-4',
  },
  {
    text: '1/2 (Long) Pan, 6" Deep',
    value: 'halfLong-6',
  },
  {
    text: '1/2 (Long) Pan, 8" Deep',
    value: 'halfLong-8',
  },
  {
    text: '1/4 Pan, 2" Deep',
    value: 'oneFourth-2',
  },
  {
    text: '1/4 Pan, 4" Deep',
    value: 'oneFourth-4',
  },
  {
    text: '1/4 Pan, 6" Deep',
    value: 'oneFourth-6',
  },
  {
    text: '2/3 Pan, 1" Deep',
    value: 'twoThird-1',
  },
  {
    text: '2/3 Pan, 2" Deep',
    value: 'twoThird-2',
  },
  {
    text: '2/3 Pan, 4" Deep',
    value: 'twoThird-4',
  },
  {
    text: '2/3 Pan, 6" Deep',
    value: 'twoThird-6',
  },
  {
    text: '2/3 Pan, 8" Deep',
    value: 'twoThird-8',
  },
  {
    text: '1/3 Pan, 1" Deep',
    value: 'oneThird-1',
  },
  {
    text: '1/3 Pan, 2" Deep',
    value: 'oneThird-2',
  },
  {
    text: '1/3 Pan, 4" Deep',
    value: 'oneThird-4',
  },
  {
    text: '1/3 Pan, 6" Deep',
    value: 'oneThird-6',
  },
  {
    text: '1/3 Pan, 8" Deep',
    value: 'oneThird-8',
  },
  {
    text: '1/6 Pan, 2" Deep',
    value: 'oneSixth-2',
  },
  {
    text: '1/6 Pan, 4" Deep',
    value: 'oneSixth-4',
  },
  {
    text: '1/6 Pan, 6" Deep',
    value: 'oneSixth-6',
  },
  {
    text: '1/9 Pan, 4" Deep',
    value: 'oneNinth-4',
  },
  {
    text: '1/9 Pan, 6" Deep',
    value: 'oneNinth-6',
  },
];

export default panOptions;
