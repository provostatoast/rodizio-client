export default [
  {
    text: '1 Serving/Person',
    value: 1,
  },
  {
    text: '2 Servings/Person',
    value: 2,
  },
  {
    text: '1/2 Serving/Person',
    value: 0.5,
  },
];
