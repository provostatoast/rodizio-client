export default [
  {
    text: 'oz',
    value: 'oz',
  },
  {
    text: 'lb',
    value: 'lb',
  },
  {
    text: 'Gram',
    value: 'g',
  },
  {
    text: 'Package/Container',
    value: 'unit',
  },
];
