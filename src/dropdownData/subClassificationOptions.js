export default [
  {
    text: 'N/A',
    value: 'na',
  },
  {
    text: 'Beef',
    value: 'beef',
  },
  {
    text: 'Pork',
    value: 'pork',
  },
  {
    text: 'Chicken',
    value: 'chicken',
  },
  {
    text: 'Lamb',
    value: 'lamb',
  },
  {
    text: 'Vegetable',
    value: 'vegetable',
  },
  {
    text: 'Fruit',
    value: 'fruit',
  },
  {
    text: 'Sauce',
    value: 'sauce',
  },
];
