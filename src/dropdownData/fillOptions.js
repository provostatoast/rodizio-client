export default [
  {
    text: 'Quarters',
    value: 0.25,
  },
  {
    text: 'Third',
    value: 0.3333,
  },
  {
    text: 'Half',
    value: 0.5,
  },
  {
    text: 'Two Thirds',
    value: 0.6666,
  },
  {
    text: 'Three Quarters',
    value: 0.75,
  },
  {
    text: 'Full',
    value: 1,
  },
];
