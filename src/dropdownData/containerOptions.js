import panOptions from './panOptions';

const containers = [
  {
    text: 'Plan B Bag',
    value: 'plan-b',
  },
  {
    text: 'Side Shovel Rack',
    value: 'side-shovel-rack',
  },
  {
    text: 'Side Shovel Rack (Charcoal Handle)',
    value: 'side-shovel-rack-charcoal',
  },
];

export default containers.concat(panOptions);
