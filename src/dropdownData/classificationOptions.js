export default [
  {
    text: 'Protein',
    value: 'protein',
  },
  {
    text: 'Topping',
    value: 'topping',
  },
  {
    text: 'Condiment',
    value: 'condiment',
  },
];
