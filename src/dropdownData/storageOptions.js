export default [
  {
    text: 'Cold',
    value: 'cold',
  },
  {
    text: 'Protein',
    value: 'protein',
  },
  {
    text: 'Dry',
    value: 'dry',
  },
  {
    text: 'Wet',
    value: 'wet',
  },
];
