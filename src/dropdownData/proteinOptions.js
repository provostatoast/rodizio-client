export default [
  {
    text: 'Beef',
    value: 'beef',
  },
  {
    text: 'Pork',
    value: 'pork',
  },
  {
    text: 'Chicken',
    value: 'chicken',
  },
  {
    text: 'Lamb',
    value: 'lamb',
  },
];
