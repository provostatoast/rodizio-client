export default [
  {
    text: 'N/A',
    value: 'na',
  },
  {
    text: 'Cold',
    value: 'cold',
  },
  {
    text: 'Hot',
    value: 'hot',
  },
  {
    text: 'Warm',
    value: 'warm',
  },
  {
    text: 'Dry',
    value: 'dry',
  },
  {
    text: 'Wet',
    value: 'wet',
  },
];
