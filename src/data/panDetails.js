export default {
  full: {
    fullEquivalents: [
      { full: 1 },
    ],
    equivalents: [
      { full: 1 },
      { half: 2 },
      { half: 1, oneFourth: 2 },
      { halfLong: 2 },
      { halfLong: 1, oneFourth: 2 },
      { halfLong: 1, oneSixth: 3 },
      { twoThird: 1, oneThird: 1 },
      { twoThird: 1, oneSixth: 2 },
      { twoThird: 1, oneNinth: 3 },
      { oneThird: 3 },
      { oneThird: 2, oneSixth: 2 },
      { oneThird: 2, oneNinth: 3 },
      { oneThird: 1, oneSixth: 4 },
      { oneThird: 1, oneSixth: 2, oneNinth: 3 },
      { oneFourth: 4 },
      { oneFourth: 2, oneSixth: 3 },
      { oneSixth: 6 },
      { oneSixth: 4, oneNinth: 3 },
      { oneSixth: 2, oneNinth: 6 },
      { oneNinth: 9 },
    ],
    capacity: [278, 447, 667, 895],
  },
  half: {
    fullEquivalents: [
      { half: 2 },
      { half: 1, oneFourth: 2 },
    ],
    equivalents: [
      { half: 1 },
      { oneFourth: 2 },
    ],
    capacity: [143, 219, 337, 447],
  },
  halfLong: {
    fullEquivalents: [
      { halfLong: 2 },
      { halfLong: 1, oneFourth: 2 },
      { halfLong: 2, oneSixth: 3 },
    ],
    equivalents: [
      { halfLong: 1 },
      { oneFourth: 2 },
      { oneSixth: 3 },
    ],
    capacity: [143, 219, 337, 447],
  },
  twoThird: {
    fullEquivalents: [
      { twoThird: 1, oneThird: 1 },
      { twoThird: 1, oneSixth: 2 },
      { twoThird: 1, oneNinth: 3 },
    ],
    equivalents: [
      { twoThird: 1 },
      { oneThird: 2 },
      { oneSixth: 4 },
      { oneNinth: 6 },
    ],
    capacity: [185, 295, 447, 590],
  },
  oneThird: {
    fullEquivalents: [
      { oneThird: 3 },
      { oneThird: 2, oneSixth: 2 },
      { oneThird: 2, oneNinth: 3 },
      { oneThird: 1, oneSixth: 4 },
      { oneThird: 1, oneNinth: 6 },
      { oneThird: 1, oneSixth: 2, oneNinth: 3 },
    ],
    equivalents: [
      { oneThird: 1 },
      { oneSixth: 2 },
      { oneNinth: 3 },
    ],
    capacity: [92, 152, 219, 295],
  },
  oneFourth: {
    fullEquivalents: [
      { oneFourth: 4 },
    ],
    equivalents: [
      { oneFourth: 1 },
    ],
    capacity: [67, 109, 168, 304],
  },
  oneSixth: {
    fullEquivalents: [
      { oneSixth: 6 },
      { oneSixth: 4, oneNinth: 3 },
      { oneSixth: 2, oneNinth: 6 },
    ],
    equivalents: [
      { oneSixth: 1 },
    ],
    capacity: [50, 76, 109, 152],
  },
  oneNinth: {
    fullEquivalents: [
      { oneNinth: 9 },
    ],
    equivalents: [
      { oneNinth: 1 },
    ],
    capacity: [33, 50, 0, 0],
  },
};
