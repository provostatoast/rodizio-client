export default [
  { name: 'double', width: 648, length: 528 },
  { name: 'full', width: 324, length: 528 },
  { name: 'half', width: 324, length: 264 },
  { name: 'halfLong', width: 162, length: 528 },
  { name: 'twoThird', width: 324, length: 352 },
  { name: 'oneThird', width: 324, length: 176 },
  { name: 'oneFourth', width: 162, length: 264 },
  { name: 'oneSixth', width: 162, length: 162 },
  { name: 'oneNinth', width: 108, length: 162 },
];
