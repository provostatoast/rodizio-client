import { largestConfigForArea } from '../panConfigurations';

export default {
  roadcase: {
    bay1: {
      width: 324,
      length: 704,
      depth: 8,
      area: () => {
        return 324 * 704 * 8;
      },
      largestConfig: () => {
        return largestConfigForArea(324, 704);
      },
    },
    bay2: {
      width: 324,
      length: 704,
      depth: 8,
      area: () => {
        return 324 * 704 * 8;
      },
      largestConfig: () => {
        return largestConfigForArea(324, 704);
      },
    },
    bay3: {
      width: 324,
      length: 704,
      depth: 8,
      area: () => {
        return 324 * 704 * 8;
      },
      largestConfig: () => {
        return largestConfigForArea(324, 704);
      },
    },
    bay4: {
      width: 324,
      length: 704,
      depth: 8,
      area: () => {
        return 324 * 704 * 8;
      },
      largestConfig: () => {
        return largestConfigForArea(324, 704);
      },
    },
    bay5: {
      width: 324,
      length: 704,
      depth: 8,
      area: () => {
        return 324 * 704 * 8;
      },
      largestConfig: () => {
        return largestConfigForArea(324, 704);
      },
    },
    bay6: {
      width: 324,
      length: 704,
      depth: 8,
      area: () => {
        return 324 * 704 * 8;
      },
      largestConfig: () => {
        return largestConfigForArea(324, 704);
      },
    },
  },
};
