/* eslint class-methods-use-this:0 */
/* eslint react/prefer-stateless-function:0 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Dropdown, Input, Button } from 'semantic-ui-react'

import './Intro.css';

const salutationOptions = [
  { key: 'mr', text: 'Sir Handsome', value: 'mr' },
  { key: 'mrs', text: 'Her Ladyship', value: 'mrs' },
];

class IntroContainer extends Component {
  constructor() {
    super();
    this.state = {
      salutation: 'MISTER',
    };
  }

  render() {
    return (
      <div>
        <p>
          Allow me to welcome you to your new favorite flavor portal jam over to the other side of food, my friend.
        </p>
        <p>
          Before we begin, please be kind and introduce yourself.
        </p>
        <p>
          Who are you?
        </p>
        <p>
          {'More importantly for legal reasons, how do we really know it\'s you from here?'}
        </p>
        <p>
          What name shall ye giveth thee?
        </p>
        <p>
          (#CRProTip001)
          THE MOST IMPORTANT WORD IN THE ENGLISH LANGUAGE IS YOUR NAME, SO MAKE IT GOOD!
        </p>
        <div className="mb20">
          <Dropdown inline floating placeholder="Title" options={salutationOptions} />
          <Input
            placeholder="Name"
          />
        </div>
        <div className="mb20">
          <Button positive>Press Enter When Ready</Button>
        </div>
        <p>{`I kid I kid. If that's the name you want to wear on your chest from here on out then by all means Rock it Out Loud and Proud Big Bad World Rodizio Voodoo Grillin ${this.state.salutation}!`}</p>
        <p>
          (#CRProTip002)
          WHERE YOUR NAME LIKE YOU OWN IT.
          DRIVE IT LIKE YOU STOLE IT!
          JUST REMEMBER THE JOKE BETTER BE GOOD, CUZ YOUR ABOUT TO HEAR IT FOR THE REST OF YOUR LIFE!
        </p>
        <div className="mb20">
          <Button positive>PRESS ENTER WHEN SURE THIS IS WHO YOU REALLY ARE</Button>
        </div>
        <div className="mb20">
          <p>Congratulations</p>
          <p>Our formal introduction has been completed</p>
        </div>
        <div className="mb20">
          Brace yourself!
        </div>
        <div className="mb20">
          Grab the biggest attention span you can before we swan dive in this jam.
        </div>
        <div className="mb20">
          On a scale of 1 to 10 how distracted are you right now?
        </div>
        <div className="mb20">
          {'One being, "I\'ve got more money than time, so let\'s do this!"'}
        </div>
        <div className="mb20">
          To 10 Being,
        </div>
        <div className="mb20">
          HOLY SHIT... my Pop-Tarts are burning!
        </div>
        <div className="mb20">
          Someone call an ambulance damn it, this strawberry jam is scalding my brain, man!
        </div>
      </div>
    );
  }
}

IntroContainer.propTypes = {
};

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  // setRangeValues: bindActionCreators(setRangeValues, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(IntroContainer);
