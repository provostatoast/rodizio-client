/* eslint class-methods-use-this:0 */
/* eslint max-len:0 */
/* eslint object-curly-newline:0 */
/* eslint react/prop-types:0 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addSavedItemMessage } from '../../reducers/messages';
import api from '../../api';
import { titleCase } from '../../utility';
import defaultState from '../../defaultStates/ingredient';
import IngredientForm from '../../components/IngredientForm';

class EditIngredient extends Component {
  constructor(props) {
    super(props);
    this.state = defaultState;

    this.handleFormSubmit = async (e) => {
      e.preventDefault();
      const {
        name,
        classification,
        subClassification,
        stored,
        prepped,
        cooked,
        served,
        pan,
        amt,
        unit,
        qty,
        price,
        ouncesPerPerson,
        useSkewers,
        panAmount,
      } = this.state;
      const message = {
        type: 'Ingredient',
        message: `${this.type} Saved!`,
      };
      const payload = {
        _id: this.state.ingredient._id,
        name: titleCase(name),
        classification,
        subClassification,
        stored,
        prepped,
        cooked,
        served,
        pan,
        amt,
        unit,
        qty,
        price,
        ouncesPerPerson,
        useSkewers,
        panAmount,
      };
      api.updateIngredient(payload)
        .then((resp) => {
          this.clearForm();
          console.log(resp);
          const { from } = this.props.location.state || {
            from: { pathname: '/ingredients' },
          };
          this.props.addSavedItemMessage(message);
          this.props.history.push(from.pathname);
        })
        .catch((err) => {
          this.clearForm();
          throw err;
          // handle error logic here
        });
    };
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      api.getIngredientById(this.props.match.params.id)
        .then((ingredient) => {
          this.setState({ ingredient: ingredient.data });
        });
    }
  }

  render() {
    return (
      <IngredientForm
        type="edit"
        ingredient={this.state.ingredient}
        handleFormSubmit={this.handleFormSubmit}
      />
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  addSavedItemMessage: bindActionCreators(addSavedItemMessage, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditIngredient);
