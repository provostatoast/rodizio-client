/* eslint class-methods-use-this:0 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import RangeComponent from '../../components/RangeComponent';
import IngredientLegend from '../../components/IngredientLegend';
import { calculateIngredientsPercentages, assignColorsToIngredients } from '../../utility';
import { setRangeValues } from '../../reducers/ranges';
import { setPercentages } from '../../reducers/percentages';

const { createSliderWithTooltip } = Slider;
const Range = createSliderWithTooltip(Slider.Range);

class RangeContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      rangeValues: this.props.rangeValues,
    };
  }

  componentWillMount() {
    const ingredients = assignColorsToIngredients(this.props.ings);

    this.setState({
      ingredients,
      showRange: true,
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ingredients: nextProps.ings,
      rangeValues: nextProps.rangeValues,
      ingredientsPercentages: nextProps.ingredientsPercentages,
    });
  }

  render() {
    if (this.props.showRange) {
      return (
        <div className="rangeComponentBlock">
          <h3>Ingredient Ratios</h3>
          <p className="helper">({ this.props.ozPerPerson } total ounces)</p>
          <div className="ratioDiv">
            <RangeComponent
              typeArray={this.state.ingredients}
              rangeValues={this.state.rangeValues}
              setRangeValues={this.props.setRangeValues}
              setPercentages={this.props.setPercentages}
            />
            <IngredientLegend
              percentages={this.props.ingredientsPercentages}
              ingredients={this.state.ingredients}
              people={this.props.people}
              ozPerPerson={this.props.ozPerPerson}
            />
          </div>
        </div>
      );
    }
    return false;
  }
}

RangeContainer.propTypes = {
  ozPerPerson: PropTypes.number.isRequired,
  people: PropTypes.number.isRequired,
  setRangeValues: PropTypes.func.isRequired,
  setPercentages: PropTypes.func.isRequired,
};

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  setRangeValues: bindActionCreators(setRangeValues, dispatch),
  setPercentages: bindActionCreators(setPercentages, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(RangeContainer);
