/* eslint class-methods-use-this:0 */
/* eslint react/prefer-stateless-function:0 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import RangeComponent from '../../components/RangeComponent';
import IngredientLegend from '../../components/IngredientLegend';
import { setRangeValues } from '../../reducers/ranges';
import { setPercentages } from '../../reducers/percentages';

class RangeContainer extends Component {
  render() {
    if (this.props.ingredients.length > 0) {
      return (
        <div className="rangeComponentBlock">
          <h3>Ingredient Ratios</h3>
          <p className="helper">({ this.props.ozPerPerson } total ounces)</p>
          <div className="ratioDiv">
            <RangeComponent
              typeArray={this.props.ingredients}
              rangeValues={this.props.rangeValues}
              setRangeValues={this.props.setRangeValues}
              setPercentages={this.props.setPercentages}
            />
            <IngredientLegend
              percentages={this.props.ingredientsPercentages}
              ingredients={this.props.ingredients}
              people={this.props.people}
              ozPerPerson={this.props.ozPerPerson}
            />
          </div>
        </div>
      );
    }
    return false;
  }
}

RangeContainer.propTypes = {
  ozPerPerson: PropTypes.number.isRequired,
  people: PropTypes.number.isRequired,
  setRangeValues: PropTypes.func.isRequired,
  setPercentages: PropTypes.func.isRequired,
};

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  setRangeValues: bindActionCreators(setRangeValues, dispatch),
  setPercentages: bindActionCreators(setPercentages, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(RangeContainer);
