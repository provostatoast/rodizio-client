import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Input, Form } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import 'rc-slider/assets/index.css';

import { setPeople } from '../../reducers/people';
import { setOzPerPerson } from '../../reducers/ozPerPerson';
import './ConfigurationForm.css';

class ConfigurationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      people: this.props.people,
      ozPerPerson: this.props.ozPerPerson,
    };

    this.updateServings = (e) => {
      this.setState({ people: e.target.value });
      props.setPeople(e.target.value);
    };

    this.updateOzPerPerson = (e) => {
      this.setState({ ozPerPerson: e.target.value });
      props.setOzPerPerson(e.target.value);
    };

    this.totalOunces = () => this.state.people * this.state.ozPerPerson;
  }

  render() {
    if (this.props.showConfiguration) {
      return (
        <div>
          <Form onSubmit={this.handleFormSubmit}>
            <Form.Field>
              <Input
                className="marginSmall"
                type="number"
                name="people"
                onChange={this.updateServings}
                label="People"
                labelPosition="right"
                value={this.state.people}
              />
            </Form.Field>
            <Form.Field>
              <Input
                className="marginSmall"
                type="number"
                name="ozPerPerson"
                onChange={this.updateOzPerPerson}
                label="Oz Per Person"
                labelPosition="right"
                value={this.state.ozPerPerson}
              />
            </Form.Field>
          </Form>
          <p><b>{`${this.totalOunces()} Total Ounces`}</b></p>
        </div>
      );
    }
    return false;
  }
}

ConfigurationForm.propTypes = {
  people: PropTypes.number.isRequired,
  ozPerPerson: PropTypes.number.isRequired,
  setPeople: PropTypes.func.isRequired,
  setOzPerPerson: PropTypes.func.isRequired,
};

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  setPeople: bindActionCreators(setPeople, dispatch),
  setOzPerPerson: bindActionCreators(setOzPerPerson, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfigurationForm);
