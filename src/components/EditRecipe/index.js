/* eslint class-methods-use-this:0 */
/* eslint max-len:0 */

import React, { Component } from 'react';
import { Dropdown, Icon, Input, Form, Button, Loader, Dimmer, Transition } from 'semantic-ui-react';
import rs from 'randomstring';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addSavedItemMessage } from '../../reducers/messages';
import { titleCase } from '../../utility';
import { setRangeValues } from '../../reducers/ranges';
import { setPercentages } from '../../reducers/percentages';
import { setIngredients } from '../../reducers/ingredients';
import api from '../../api';
import AddIngredientModal from '../Modal';
import AddTemplateModal from '../AddTemplateModal';
import RangeComponent from '../RangeComponent';
// import amountOptions from '../../dropdownData/amountOptions';

const defaultState = {};

class AddRecipe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: '',
      name: '',
      showRange: false,
      rangeButtonText: 'Edit Ingredient Ratios',
      ingredients: [],
      ingredientOptions: [],
      showLoader: true,
    };

    this.toggleSetRatio = (e) => {
      e.preventDefault();
      const rangeButtonText = this.state.showRange
        ? 'Edit Ingredient Ratios'
        : 'Close Ingredient Ratios';
      this.setState({
        showRange: !this.state.showRange,
        rangeButtonText,
      });
    };

    this.ingredientUpdate = (e, data) => {
      e.preventDefault();
      const ingredients = this.state.ingredients.map((ingredient) => {
        const ing = ingredient;
        if (data.unique === ing.uniquekey) {
          ing.value = data.value;
        }
        return ing;
      });
      this.setState({ ingredients });
    };

    this.removeIngredient = (e) => {
      e.preventDefault();
      const removeId = e.target.id;
      const ingredients = this.state.ingredients.filter(ing => ing.uniquekey !== removeId);

      this.setState({ ingredients });
    };

    this.updateName = (e) => {
      this.setState({ name: e.target.value });
    };

    this.addIngredientComponent = (e) => {
      e.preventDefault();
      const { ingredients } = this.state;
      const uniquekey = rs.generate(6);
      ingredients.push({
        _id: '',
        ingredient: null,
        uniquekey,
      });

      return this.setState({ ingredients });
    };

    this.clearForm = () => {
      this.setState(defaultState);
    };

    this.ingredientList = () => {
      const { ingredients } = this.state;
      return ingredients.map(ing => this.ingredientComponent(ing.value, ing.uniquekey));
    };

    this.showLoader = () => {
      if (this.state.showLoader) {
        return (
          <Dimmer active>
            <Loader size="large">Loading Recipe...</Loader>
          </Dimmer>
        );
      }
      return false;
    };

    this.handleFormSubmit = async (e) => {
      e.preventDefault();
      const { ingredients } = this.state;
      const recipeObj = {};
      const newIngredients = ingredients.map((ing) => {
        const _id = ing._id || '';
        const ingObj = { _id, ingredient: ing.value, amount: ing.amount };
        return ingObj;
      });

      recipeObj._id = this.state._id;
      recipeObj.name = titleCase(this.state.name);
      recipeObj.ingredients = newIngredients;
      recipeObj.rangeValues = this.props.rangeValues;
      recipeObj.ingredientsPercentages = this.props.ingredientsPercentages;

      api.updateRecipeById(recipeObj)
        .then(() => {
          const { from } = this.props.location.state || {
            from: { pathname: '/recipes' },
          };
          this.props.history.push(from.pathname);
        })
        .catch((err) => {
          this.clearForm();
          throw err;
          // handle error logic here
        });
    };

    this.getUpdatedIngredientsList = () => {
      api.getAllIngredients()
        .then((ingredients) => {
          const uniquekey = rs.generate(6);
          const options = ingredients.data;
          const ingredientOptions = options.map(opt => ({
            uniquekey,
            text: opt.name,
            value: opt._id,
          }));
          this.setState({
            ingredientOptions,
          });
        });
    };

    this.addIngredientsFromTemplate = (id) => {
      api.getRecipeById(id)
        .then((recipe) => {
          let { ingredients } = this.state;
          const recipeIngredients = recipe.data.ingredients
            .map((ing) => {
              const uniquekey = rs.generate(6);
              return ({
                value: ing.ingredient._id,
                _id: ing._id,
                uniquekey,
              });
            });

          ingredients = ingredients.concat(recipeIngredients);
          this.setState({
            ingredients,
          }, () => {
            this.ingredientList();
          });
        });
    };

    this.ingredientComponent = (val, unique) => {
      const { ingredientOptions } = this.state;
      return (
        <Form.Group widths="equal">
          <Dropdown
            inline
            selection
            options={ingredientOptions}
            unique={unique}
            value={val}
            placeholder="Select Ingredient"
            onChange={this.ingredientUpdate}
          />
          <Button id={unique} color="red" floated="right" onClick={this.removeIngredient}><Icon name="close" />Remove</Button>
        </Form.Group>
      );
    };
  }

  componentDidMount() {
    this.getUpdatedIngredientsList();
    api.getRecipeById(this.props.match.params.id)
      .then((recipe) => {
        const ingredients = recipe.data.ingredients
          .map((ing) => {
            const uniquekey = rs.generate(6);
            return ({
              ingredient: ing.ingredient,
              value: ing.ingredient._id,
              _id: ing._id,
              uniquekey,
            });
          });
        this.props.setRangeValues(recipe.data.rangeValues);
        this.props.setIngredients(recipe.data.ingredients);
        this.props.setPercentages(recipe.data.ingredientsPercentages);
        this.setState({
          _id: recipe.data._id,
          name: recipe.data.name,
          ingredients,
        }, () => {
          this.ingredientList();
        });
        this.setState({ showLoader: false });
      });
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleFormSubmit}>
          <h2 style={{ textAlign: 'center' }}>Edit Recipe</h2>
          <hr />
          <Form.Field>
            <label>Ingredient Name</label>
            <Input
              onChange={this.updateName}
              placeholder="Recipe Name"
              value={this.state.name}
            />
          </Form.Field>
          <h3>Ingredients</h3>
          <ul className="ingredients">
            { this.ingredientList() }
          </ul>
          <div style={{ overflow: 'hidden' }}>
            <Button className="floatLeft" size="small" onClick={this.addIngredientComponent}>Add Ingredient</Button>
            <AddTemplateModal className="floatLeft" addTemplateId={this.addIngredientsFromTemplate} />
            <AddIngredientModal className="floatLeft" updateItems={this.getUpdatedIngredientsList} />
          </div>
          <Button size="small" onClick={this.toggleSetRatio}>{ this.state.rangeButtonText }</Button>
          <Transition
            visible={this.state.showRange}
            duration={200}
            animation="fade"
            unmountOnHide
          >
            <div>
              <RangeComponent
                showRange="true"
                ings={this.state.ingredients}
              />
            </div>
          </Transition>
          <Button
            color="blue"
            style={{ display: 'block', margin: 'auto', textAlign: 'center' }}
            onClick={this.handleFormSubmit}
          >
            Save!
          </Button>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  addSavedItemMessage: bindActionCreators(addSavedItemMessage, dispatch),
  setRangeValues: bindActionCreators(setRangeValues, dispatch),
  setPercentages: bindActionCreators(setPercentages, dispatch),
  setIngredients: bindActionCreators(setIngredients, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddRecipe);
