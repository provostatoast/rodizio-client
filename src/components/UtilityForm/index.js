/* eslint class-methods-use-this:0 */
/* eslint max-len:0 */
/* eslint object-curly-newline:0 */

import React, { Component } from 'react';
import { Form, Input, Label, Loader, Dimmer, Dropdown } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addSavedItemMessage } from '../../reducers/messages';
import { titleCase } from '../../utility';
import defaultState from '../../defaultStates/utensil';
import containerOptions from '../../dropdownData/containerOptions';

const translateToNumber = ['defaultServing', 'amt', 'qty', 'panAmount'];

class EditUtility extends Component {
  constructor(props) {
    super(props);
    this.state = defaultState;

    this.loader = () => {
      if (this.state.showLoader) {
        return (
          <Dimmer active>
            <Loader size="large">Loading Utility Data...</Loader>
          </Dimmer>
        );
      }
      return false;
    };

    this.showLoader = () => (
      this.setState({ showLoader: true })
    );

    this.clearForm = () => {
      this.setState(defaultState);
    };

    this.updateForm = (e, data) => {
      const valObj = {};
      const indexInTranslateArray = translateToNumber.indexOf(data.update);

      valObj[data.update] = indexInTranslateArray > -1
        ? Number(data.value)
        : data.value;

      this.setState(valObj);
    };

    this.showState = () => {
      console.log(this.state);
    };

    this.setUtilityToState = (u) => {
      const {
        name,
        image,
        cost,
        link,
      } = u;

      this.setState({
        name: titleCase(name),
        image,
        cost,
        link,
      }, () => {
        this.setState({ showLoader: false });
      });
    };
  }

  componentDidMount() {
    if (this.props.type === 'edit') {
      this.showLoader();
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setUtilityToState(nextProps.utensil);
  }

  render() {
    return (
      <div>
        { this.loader() }
        <Form onSubmit={this.props.handleFormSubmit}>
          <h2 style={{ textAlign: 'center' }}>Edit { this.state.name }</h2>
          <hr />
          <Form.Field required>
            <Label>Utility Name</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Utility Name"
              update="name"
              value={this.state.name}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Utility Image</Label>
            <img src={this.state.image} alt="Utility" />
            <Input
              onChange={this.updateForm}
              placeholder="Utility Image URL"
              update="image"
              value={this.state.image}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Average Package Size (Oz)</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Package Size (oz)"
              update="oz"
              value={this.state.oz}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Cost Per Oz</Label>
            <Input
              label="$"
              onChange={this.updateForm}
              placeholder="Cost Per Oz"
              update="costPerOz"
              value={this.state.costPerOz}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Cost Per Package</Label>
            <Input
              label="$"
              onChange={this.updateForm}
              labelPosition="left"
              type="number"
              update="costPerPackage"
              value={this.state.costPerPackage}
              placeholder="Cost Per Package"
            />
          </Form.Field>
          <Form.Field required>
            <Label>Utility Link</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Utility URL"
              update="link"
              value={this.state.link}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Final Resting Container</Label>
            <Dropdown
              options={containerOptions}
              onChange={this.updateForm}
              selection
              search
              update="container"
              value={this.state.container}
            />
          </Form.Field>
          <Input
            type="submit"
            value="Submit!"
            // onClick={this.handleFormSubmit}
            style={{ display: 'block', margin: 'auto' }}
          />
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  addSavedItemMessage: bindActionCreators(addSavedItemMessage, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditUtility);
