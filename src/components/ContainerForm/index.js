/* eslint class-methods-use-this:0 */
/* eslint max-len:0 */
/* eslint object-curly-newline:0 */

import React, { Component } from 'react';
import { Form, Input, Label, Loader, Dimmer } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addSavedItemMessage } from '../../reducers/messages';
import { titleCase } from '../../utility';
import defaultState from '../../defaultStates/utensil';

const translateToNumber = ['defaultServing', 'amt', 'qty', 'panAmount'];

class EditContainer extends Component {
  constructor(props) {
    super(props);
    this.state = defaultState;

    this.loader = () => {
      if (this.state.showLoader) {
        return (
          <Dimmer active>
            <Loader size="large">Loading Container Data...</Loader>
          </Dimmer>
        );
      }
      return false;
    };

    this.showLoader = () => (
      this.setState({ showLoader: true })
    );

    this.clearForm = () => {
      this.setState(defaultState);
    };

    this.updateForm = (e, data) => {
      const valObj = {};
      const indexInTranslateArray = translateToNumber.indexOf(data.update);

      valObj[data.update] = indexInTranslateArray > -1
        ? Number(data.value)
        : data.value;

      this.setState(valObj);
    };

    this.showState = () => {
      console.log(this.state);
    };

    this.setContainerToState = (u) => {
      const {
        name,
        image,
        cost,
        link,
      } = u;

      this.setState({
        name: titleCase(name),
        image,
        cost,
        link,
      }, () => {
        this.setState({ showLoader: false });
      });
    };
  }

  componentDidMount() {
    if (this.props.type === 'edit') {
      this.showLoader();
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setContainerToState(nextProps.utensil);
  }

  render() {
    return (
      <div>
        { this.loader() }
        <Form onSubmit={this.props.handleFormSubmit}>
          <h2 style={{ textAlign: 'center' }}>Edit { this.state.name }</h2>
          <hr />
          <Form.Field required>
            <Label>Container Name</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Container Name"
              update="name"
              value={this.state.name}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Container Image</Label>
            <img src={this.state.image} alt="Container" />
            <Input
              onChange={this.updateForm}
              placeholder="Container Image URL"
              update="image"
              value={this.state.image}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Capacity in Oz (fluid)</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Ounces"
              update="oz"
              value={this.state.oz}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Capacity in Quarts (fluid)</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Quarts"
              update="quarts"
              value={this.state.oz}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Cost for Container</Label>
            <Input
              label="$"
              onChange={this.updateForm}
              labelPosition="left"
              type="number"
              update="cost"
              value={this.state.cost}
              placeholder="Price Per Container"
            />
          </Form.Field>
          <Form.Field required>
            <Label>Container Link</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Container URL"
              update="link"
              value={this.state.link}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Lid Name</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Lid Name"
              update="lidName"
              value={this.state.lidName}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Cost for Lid</Label>
            <Input
              label="$"
              onChange={this.updateForm}
              labelPosition="left"
              type="number"
              update="lidCost"
              value={this.state.lidCost}
              placeholder="Price Per Container"
            />
          </Form.Field>
          <Form.Field required>
            <Label>Lid Link</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Container URL"
              update="lidLink"
              value={this.state.lidLink}
            />
          </Form.Field>
          <Input
            type="submit"
            value="Submit!"
            // onClick={this.handleFormSubmit}
            style={{ display: 'block', margin: 'auto' }}
          />
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  addSavedItemMessage: bindActionCreators(addSavedItemMessage, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer);
