import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { List, Dimmer, Loader, Button, Icon } from 'semantic-ui-react';

import api from '../../api';
// import SocialLoginLinks from './SocialLoginLinks';

class Recipes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recipes: [],
      showLoader: true,
    };

    this.editRecipe = (e) => {
      e.preventDefault();
      const recipeId = e.target.getAttribute('recipeId');
      const { from } = this.props.location.state || {
        from: { pathname: `/recipes/edit/${recipeId}` },
      };
      this.props.history.push(from.pathname);
    };

    this.removeRecipe = (e) => {
      e.preventDefault();
      const { id } = e.target;
      api.deleteRecipe(id)
        .then((err) => {
          if (err) console.log(err);

          const recipes = this.state.recipes.filter(recipe => recipe._id !== id);
          this.setState({ recipes });
        });
    };
  }

  componentDidMount() {
    api.getAllRecipes()
      .then((recipes) => {
        console.log(recipes);
        this.setState({ recipes: recipes.data });
        this.setState({ showLoader: false });
      });
  }

  showLoader() {
    if (this.state.showLoader) {
      return (
        <Dimmer active>
          <Loader size="large">Loading Recipes...</Loader>
        </Dimmer>
      );
    }
    return false;
  }

  renderRecipes() {
    const lis = this.state.recipes.map((recipe, i) => {
      const properName = recipe.name;
      const key = `item-${i}`;
      return (
        <List.Item key={key}>
          <List.Content floated="left">
            <Link className="listItemName" to={`/recipes/recipe/${recipe._id}`}>{properName}</Link>
          </List.Content>
          <List.Content floated="right">
            <List.Content floated="right">
              <Button
                id={recipe._id}
                onClick={this.removeRecipe}
                color="red"
              >
                <Icon name="close" /> Delete
              </Button>
            </List.Content>
            <List.Content floated="right">
              <Button
                as={Link}
                to={`/recipes/edit/${recipe._id}`}
                recipeId={recipe._id}
                onClick={this.editRecipe}
              >
                <Icon name="edit" /> Edit
              </Button>
            </List.Content>
          </List.Content>
        </List.Item>
      );
    });

    return (
      <List divided verticalAlign="middle">{ lis }</List>
    );
  }

  render() {
    return (
      <div>
        { this.showLoader() }
        <h1>All Recipes</h1>
        { this.renderRecipes() }
        <Button as={Link} to="/recipes/new/">Add New Recipe</Button>
      </div>
    );
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps, null)(Recipes);
