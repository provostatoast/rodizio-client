/* eslint class-methods-use-this:0 */
/* eslint max-len:0 */

import React, { Component } from 'react';
import { Dropdown, Input, Form, Button, Transition, Icon } from 'semantic-ui-react';
import rs from 'randomstring';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addSavedItemMessage } from '../../reducers/messages';
import { titleCase, setRangeValueArray } from '../../utility';
import { setRangeValues } from '../../reducers/ranges';
import { setPercentages } from '../../reducers/percentages';
import { setIngredients } from '../../reducers/ingredients';
import api from '../../api';
import AddIngredientModal from '../Modal';
import AddTemplateModal from '../AddTemplateModal';
import RangeComponent from '../RangeComponent';
import './AddRecipe.css';
// import amountOptions from '../../dropdownData/amountOptions';

const defaultState = {};

class AddRecipe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      // allIngredients: {},
      showRange: false,
      rangeButtonText: 'Show Range Values',
      ingredients: [],
      ingredientOptions: [],
    };

    this.toggleSetRatio = (e) => {
      e.preventDefault();
      const rangeButtonText = this.state.showRange
        ? 'Show Range Ranges'
        : 'Hide Range Values';
      this.setState({
        showRange: !this.state.showRange,
        rangeButtonText,
      });
    };

    this.ingredientUpdate = (e, data) => {
      e.preventDefault();
      // console.log(this.state.allIngredients);
      const ingredients = this.state.ingredients.map((ingredient) => {
        const ing = ingredient;
        if (data.unique === ing.uniquekey) {
          ing.value = data.value;
          const matchingObjs = this.state.ingredientOptions
            .filter(opt => opt.ingredient._id === data.value);
          ing.ingredient = matchingObjs[0].ingredient;
        }
        return ing;
      });
      const rangeVals = setRangeValueArray(this.state.ingredients);
      const { rangeValues, ingredientsPercentages } = rangeVals;

      this.props.setRangeValues(rangeValues);
      this.props.setPercentages(ingredientsPercentages);
      this.props.setIngredients(ingredients);
      this.setState({ ingredients });
    };

    this.removeIngredient = (e) => {
      e.preventDefault();
      const removeId = e.target.id;
      const ingredients = this.state.ingredients.filter(ing => ing.uniquekey !== removeId);

      this.setState({ ingredients });
      this.props.setIngredients(ingredients);
    };

    this.updateName = (e) => {
      this.setState({ name: e.target.value });
    };

    this.addIngredientComponent = (e) => {
      e.preventDefault();
      const { ingredients } = this.state;
      const uniquekey = rs.generate(6);
      ingredients.push({ ingredient: null, uniquekey });

      return this.setState({ ingredients });
    };

    this.showSubmit = () => {
      if (!this.props.setup) {
        return (
          <Button
            color="blue"
            style={{ display: 'block', margin: 'auto', textAlign: 'center' }}
            onClick={this.handleFormSubmit}
          >
            Submit
          </Button>
        );
      }
      return false;
    };

    this.showRangeToggleButton = () => {
      if (!this.props.setup) {
        return (
          <Button size="small" onClick={this.toggleSetRatio}>{ this.state.rangeButtonText }</Button>
        );
      }
      return false;
    };

    this.showHeader = () => {
      if (!this.props.setup) {
        return (
          <div>
            <h2>Add Recipe</h2>
            <Form.Field>
              <label>Recipe Name</label>
              <Input
                onChange={this.updateName}
                placeholder="Recipe Name"
                value={this.state.name}
              />
            </Form.Field>
          </div>
        );
      }
      return false;
    }

    this.clearForm = () => {
      this.setState(defaultState);
    };

    this.ingredientList = () => {
      const { ingredients } = this.state;
      if (ingredients.length > 0) {
        return ingredients.map(ing => this.ingredientComponent(ing.value, ing.uniquekey));
      }
      return (<p><i>None added yet...</i></p>);
    };

    this.handleFormSubmit = async (e) => {
      e.preventDefault();
      const { ingredients } = this.state;
      const recipeObj = {};
      const newIngredients = ingredients.map((ing) => {
        const ingObj = { ingredient: ing.value, amount: ing.amount };
        return ingObj;
      });

      recipeObj.name = titleCase(this.state.name);
      recipeObj.ingredients = newIngredients;
      recipeObj.rangeValues = this.props.rangeValues;
      recipeObj.ingredientsPercentages = this.props.ingredientsPercentages;

      api.addNewRecipe(recipeObj)
        .then(() => {
          const { from } = this.props.location.state || {
            from: { pathname: '/recipes' },
          };
          this.props.setIngredients([]);
          this.props.history.push(from.pathname);
        })
        .catch((err) => {
          this.clearForm();
          throw err;
          // handle error logic here
        });
    };

    this.getUpdatedIngredientsList = () => {
      api.getAllIngredients()
        .then((ingredients) => {
          const options = ingredients.data;
          const ingredientOptions = options.map((opt) => {
            const uniquekey = rs.generate(6);
            return ({
              uniquekey,
              ingredient: opt,
              text: opt.name,
              value: opt._id,
            });
          });
          this.setState({
            ingredientOptions,
          });
        });
    };

    this.addIngredientsFromTemplate = (id) => {
      api.getRecipeById(id)
        .then((recipe) => {
          let { ingredients } = this.state;
          const recipeIngredients = recipe.data.ingredients
            .map((ing) => {
              const uniquekey = rs.generate(6);
              return ({
                ingredient: ing.ingredient,
                value: ing.ingredient._id,
                uniquekey,
              });
            });

          ingredients = ingredients.concat(recipeIngredients);
          const rangeVals = setRangeValueArray(ingredients);
          const { rangeValues, ingredientsPercentages } = rangeVals;

          this.props.setRangeValues(rangeValues);
          this.props.setPercentages(ingredientsPercentages);
          this.setState({
            ingredients,
          }, () => {
            this.props.setIngredients(ingredients).then(() => {
              this.ingredientList();
            });
          });
        });
    };

    this.ingredientComponent = (val, unique) => {
      const { ingredientOptions } = this.state;
      return (
        <Form.Group key={unique}>
          <Dropdown
            inline
            selection
            options={ingredientOptions}
            unique={unique}
            value={val}
            placeholder="Select Ingredient"
            onChange={this.ingredientUpdate}
          />
          <Button basic id={unique} color="red" floated="right" onClick={this.removeIngredient}><Icon name="close" />Remove</Button>
        </Form.Group>
      );
    };
  }

  componentDidMount() {
    // console.log(this.props.location.pathname);
    this.getUpdatedIngredientsList();
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleFormSubmit}>
          { this.showHeader() }
          <h2>Ingredients</h2>
          <ul className="ingredients">
            { this.ingredientList() }
          </ul>
          <div style={{ overflow: 'hidden' }}>
            <Button className="floatLeft" size="small" onClick={this.addIngredientComponent}>Add Ingredient</Button>
            <AddTemplateModal className="floatLeft" addTemplateId={this.addIngredientsFromTemplate} />
          </div>
          { this.showRangeToggleButton() }
          <Transition
            visible={this.state.showRange}
            duration={200}
            animation="fade"
            unmountOnHide
          >
            <div className="rangeComponentBlock">
              <RangeComponent
                ings={this.state.ingredients}
              />
            </div>
          </Transition>
          { this.showSubmit() }
        </Form>

        <AddIngredientModal updateItems={this.getUpdatedIngredientsList} />
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  addSavedItemMessage: bindActionCreators(addSavedItemMessage, dispatch),
  setRangeValues: bindActionCreators(setRangeValues, dispatch),
  setPercentages: bindActionCreators(setPercentages, dispatch),
  setIngredients: bindActionCreators(setIngredients, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddRecipe);
