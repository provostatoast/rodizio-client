import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, Button } from 'semantic-ui-react';
import { showModal, hideModal } from '../../actions/modal';

import AddIngredient from '../AddIngredient';
import './Modal.css';

class AddIngredientModal extends Component {
  constructor(props) {
    super(props);
    this.handleOpen = () => this.props.showModal();
    this.handleClose = () => {
      this.props.hideModal();
      return this.props.updateItems();
    };
  }
  render() {
    return (
      <div className="addIngredientModal">
        <p>
          Need to add another ingredient to the list of options? <Button basic size="small" color="blue" onClick={this.handleOpen}>Create Ingredient</Button>
        </p>
        <Modal
          open={this.props.modalDisplay}
          onClose={this.handleClose}
        >
          <Modal.Header>Create Ingredient</Modal.Header>
          <Modal.Content>
            <AddIngredient onClose={this.handleClose} />
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  showModal: bindActionCreators(showModal, dispatch),
  hideModal: bindActionCreators(hideModal, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddIngredientModal);
