/* eslint react/prop-types:0 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
// import { Menu } from 'semantic-ui-react';

import { reAuthUser } from '../../reducers/auth';
// import Home from '../Home';
import LoginContainer from '../Login';
import RegisterContainer from '../Register';
import Setup from '../Setup';
import Intro from '../../containers/Intro';
import RecipesContainer from '../Recipes';
import AddRecipeContainer from '../AddRecipe';
import IngredientsContainer from '../../components/Ingredients';
import AddIngredientsContainer from '../AddIngredient';
import EditIngredientsContainer from '../../containers/EditIngredient';
import EditRecipeContainer from '../EditRecipe';
import IndividualRecipeContainer from '../../components/IndividualRecipe';
import Menu from '../Menu';
import './App.css';

class App extends Component {
  componentWillMount() {
    // this.props.dispatch(reAuthUser(() => this.props.history.push('/login')));
  }

  render() {
    return (
      <Router>
        <div>
          <Menu />
          <div className="container">
            <Route exact path="/" component={Setup} />
            <Route exact path="/introduction" component={Intro} />
            <Route path="/login" component={LoginContainer} />
            <Route path="/register" component={RegisterContainer} />

            <Route exact path="/recipes" component={RecipesContainer} />
            <Route exact path="/configuration/" component={IndividualRecipeContainer} />
            <Route path="/recipes/recipe/:name" component={IndividualRecipeContainer} />
            <Route exact path="/recipes/new/" component={AddRecipeContainer} />
            <Route path="/recipes/edit/:id" component={EditRecipeContainer} />

            <Route exact path="/ingredients/" component={IngredientsContainer} />
            <Route path="/ingredients/new/" component={AddIngredientsContainer} />
            <Route path="/ingredients/edit/:id" component={EditIngredientsContainer} />
          </div>
        </div>
      </Router>
    );
  }
}

const AppContainer = connect()(App);

export default AppContainer;
