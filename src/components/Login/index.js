import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginUser } from '../../reducers/auth';
import { setPeople } from '../../reducers/people';

class Login extends Component {
  constructor(props) {
    super(props);

    this.clearForm = () => {
      this.email.value = '';
      this.password.value = '';
    };

    this.handleFormSubmit = async (e) => {
      e.preventDefault();
      const payload = {
        email: this.email.value,
        password: this.password.value,
      };
      this.props.setPeople(this.people.value);
      this.props.loginUser(payload, () => {
        this.clearForm();
        const { from } = this.props.location.state || {
          from: { pathname: '/recipes' },
        };
        this.props.history.push(from.pathname);
      });
    };
  }
  render() {
    console.log(this.props);
    const { loginErrors } = this.props.authReducer;

    return (
      <div>
        <form onSubmit={this.handleFormSubmit}>
          <h2 style={{ textAlign: 'center' }}>Login</h2>
          <hr />
          <input
            type="number"
            ref={(node) => {
              this.people = node;
            }}
            maxLength="50"
            style={{ margin: 10 }}
            placeholder="How Many People"
          />
          Does
          <input
            type="email"
            ref={(node) => {
              this.email = node;
            }}
            required
            maxLength="50"
            style={{ margin: 10 }}
            placeholder="Username"
          />
          <span style={{ color: 'red' }}>*</span>
          <span style={{ color: 'red', marginLeft: 8 }}>
            {loginErrors.email}
          </span>
          <br />
          want to feed today?
          <input
            type="password"
            ref={(node) => {
              this.password = node;
            }}
            required
            maxLength="50"
            style={{ margin: 10 }}
            placeholder="Password..."
          />
          <span style={{ color: 'red' }}>*</span>
          <span style={{ color: 'red', marginLeft: 8 }}>
            {loginErrors.password}
          </span>
          <br />

          <input
            type="submit"
            value="Submit!"
            style={{ display: 'block', margin: 'auto' }}
          />
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  setPeople: bindActionCreators(setPeople, dispatch),
  loginUser: bindActionCreators(loginUser, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
