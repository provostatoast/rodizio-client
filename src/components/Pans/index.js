import React from 'react';
import './Pans.css';

const Pans = () => (
  <div className="bay">
    {/* <div className="full white">full</div> */}

    {/* <div className="pan half">half</div>
    <div className="pan half">half</div> */}

    {/* <div className="pan twoThird">two third</div>
    <div className="pan twoThird">two third</div> */}

    {/* <div className="pan oneThird">one third</div>
    <div className="pan oneThird">one third</div>
    <div className="pan oneThird">one third</div>
    <div className="pan oneThird">one third</div> */}

    {/* <div className="pan oneSixth">one sixth</div>
    <div className="pan oneSixth">one sixth</div>
    <div className="pan oneSixth">one sixth</div>
    <div className="pan oneSixth">one sixth</div>
    <div className="pan oneSixth">one sixth</div>
    <div className="pan oneSixth">one sixth</div>
    <div className="pan oneSixth">one sixth</div>
    <div className="pan oneSixth">one sixth</div> */}

    {/* <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div> */}

    <div className="pan twoThird">two third</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneNinth">one ninth</div>
    <div className="pan oneSixth white">one sixth</div>
    <div className="pan oneSixth white">one sixth</div>
  </div>
);

export default Pans;
