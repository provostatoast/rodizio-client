import React, { Component } from 'react';
import { Menu, Modal, Button, Dropdown, Input } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import ConfigurationForm from '../ConfigurationForm';

export default class MenuExampleBasic extends Component {
  constructor() {
    super();
    this.state = {
      activeItem: '',
      configurationModal: false,
      showForm: false,
    };
    // this.toggleConfigurationModal = () => {
    //   this.setState({ configurationModal: !this.state.configurationModal });
    // };
    this.handleItemClick = (e, { name }) => {
      this.setState({ activeItem: name });
    };
    // this.toggleConfiguration = () => {
    //   this.setState({ showForm: !this.state.showForm });
    // };
  }

  render() {
    const { activeItem } = this.state;

    return (
      <Menu closeonblur="false">
        <Menu.Item
          as={Link}
          to="/"
          name="home"
          active={activeItem === 'home'}
          onClick={this.handleItemClick}
        >
          Home
        </Menu.Item>

        <Menu.Menu position="right">
          <Menu.Item
            as={Link}
            to="/recipes"
            name="recipes"
            active={activeItem === 'recipes'}
            onClick={this.handleItemClick}
          >
            Recipes
          </Menu.Item>

          <Menu.Item
            as={Link}
            to="/ingredients"
            name="ingredients"
            active={activeItem === "ingredients"}
            onClick={this.handleItemClick}
          >
            Ingredients
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}
