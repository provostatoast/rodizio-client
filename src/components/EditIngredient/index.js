/* eslint class-methods-use-this:0 */
/* eslint max-len:0 */
/* eslint object-curly-newline:0 */

import React, { Component } from 'react';
import { Form, Dropdown, Input, Checkbox } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addSavedItemMessage } from '../../reducers/messages';
import api from '../../api';
import { titleCase } from '../../utility';
import panOptions from '../../dropdownData/panOptions';
import foodStateOptions from '../../dropdownData/foodStateOptions';
import classificationOptions from '../../dropdownData/classificationOptions';
import subClassificationOptions from '../../dropdownData/subClassificationOptions';
import defaultState from '../../defaultStates/ingredient';

const translateToNumber = ['defaultServing', 'amt', 'qty', 'panAmount'];

class EditIngredient extends Component {
  constructor(props) {
    super(props);
    this.state = defaultState;

    this.clearForm = () => {
      this.setState(defaultState);
    };

    this.skewers = (e, data) => {
      this.setState({ useSkewers: data.checked });
    };

    this.updateForm = (e, data) => {
      const valObj = {};
      const indexInTranslateArray = translateToNumber.indexOf(data.update);

      valObj[data.update] = indexInTranslateArray > -1
        ? Number(data.value)
        : data.value;

      this.setState(valObj);
    };

    this.showState = () => {
      console.log(this.state);
    };

    this.showIngredientSummary = () => {
      const thisMany = Math.floor(this.state.amt / this.state.ouncesPerPerson);
      const displaySummary = this.state.ouncesPerPerson && this.state.amt;
      if (displaySummary) {
        return (
          `Based on the allotted oz. per person (${this.state.ouncesPerPerson}), this many
          ounces of ${this.state.name} will feed ${thisMany} people.`
        );
      }
      return false;
    };

    this.handleFormSubmit = async (e) => {
      e.preventDefault();
      const {
        name,
        classification,
        subClassification,
        stored,
        prepped,
        cooked,
        served,
        pan,
        amt,
        unit,
        qty,
        price,
        ouncesPerPerson,
        useSkewers,
        panAmount,
      } = this.state;
      const message = {
        type: 'Ingredient',
        message: `${this.type} Saved!`,
      };
      const payload = {
        _id: this.state.ingredient._id,
        name: titleCase(name),
        classification,
        subClassification,
        stored,
        prepped,
        cooked,
        served,
        pan,
        amt,
        unit,
        qty,
        price,
        ouncesPerPerson,
        useSkewers,
        panAmount,
      };
      api.updateIngredient(payload)
        .then((resp) => {
          this.clearForm();
          console.log(resp);
          const { from } = this.props.location.state || {
            from: { pathname: '/ingredients' },
          };
          this.props.addSavedItemMessage(message);
          this.props.history.push(from.pathname);
        })
        .catch((err) => {
          this.clearForm();
          throw err;
          // handle error logic here
        });
    };
  }

  componentDidMount() {
    api.getIngredientById(this.props.match.params.id)
      .then((ingredient) => {
        console.log(ingredient);
        const {
          name,
          classification,
          subClassification,
          stored,
          prepped,
          cooked,
          served,
          pan,
          amt,
          unit,
          qty,
          price,
          ouncesPerPerson,
          useSkewers,
          panAmount,
        } = ingredient.data;

        this.setState({
          ingredient: ingredient.data,
          name: titleCase(name),
          classification,
          subClassification,
          stored,
          prepped,
          cooked,
          served,
          pan,
          amt,
          unit,
          qty,
          price,
          ouncesPerPerson,
          useSkewers,
          panAmount,
        });
      });
  }

  showSkewersOption() {
    if (this.state.name) {
      return (
        <div>
          <Checkbox checked={this.state.useSkewers} onChange={this.skewers} label="Are you using skewers for this?" />
          <br /><br />
        </div>
      );
    }
    return false;
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleFormSubmit}>
          <h2 style={{ textAlign: 'center' }}>Edit { this.state.name }</h2>
          <hr />
          <Form.Field required>
            <label>Ingredient Name</label>
            <Input
              onChange={this.updateForm}
              placeholder="Ingredient Name"
              update="name"
              value={this.state.name}
            />
          </Form.Field>
          <Form.Field required>
            <label>Pan Size it goes in</label>
            <Dropdown
              options={panOptions}
              onChange={this.updateForm}
              selection
              search
              update="pan"
              value={this.state.pan}
            />
          </Form.Field>
          <Form.Field required>
            <label>Classification</label>
            <Dropdown
              options={classificationOptions}
              onChange={this.updateForm}
              update="classification"
              selection
              value={this.state.classification}
            />
          </Form.Field>
          <Form.Field>
            <label>Sub Classification</label>
            <Dropdown
              options={subClassificationOptions}
              onChange={this.updateForm}
              update="subClassification"
              selection
              value={this.state.subClassification}
            />
          </Form.Field>
          <Form.Field required>
            <label>Stored</label>
            <Dropdown
              options={foodStateOptions}
              onChange={this.updateForm}
              update="stored"
              selection
              value={this.state.stored}
            />
          </Form.Field>
          <Form.Field required>
            <label>Prepped</label>
            <Dropdown
              options={foodStateOptions}
              onChange={this.updateForm}
              update="prepped"
              selection
              value={this.state.prepped}
            />
          </Form.Field>
          <Form.Field required>
            <label>Cooked</label>
            <Dropdown
              options={foodStateOptions}
              onChange={this.updateForm}
              update="cooked"
              selection
              value={this.state.cooked}
            />
          </Form.Field>
          <Form.Field required>
            <label>Served</label>
            <Dropdown
              options={foodStateOptions}
              onChange={this.updateForm}
              update="served"
              selection
              value={this.state.served}
            />
          </Form.Field>
          { this.showSkewersOption() }
          <Form.Field required>
            <label>What Dictates One Unit?</label>
            <Input
              onChange={this.updateForm}
              placeholder="Package, Onion, Rack..."
              update="unit"
              value={this.state.unit}
            />
          </Form.Field>
          <Form.Field required>
            <label>What is the Avg. Weight in Ounces?</label>
            <Input
              label="Oz"
              onChange={this.updateForm}
              labelPosition="right"
              type="number"
              update="amt"
              value={this.state.amt}
              placeholder="This many..."
            />
          </Form.Field>
          <Form.Field required>
            <label>How Many Oz Fits in Pan?</label>
            <Input
              type="number"
              onChange={this.updateForm}
              placeholder="Ounces in One Pan"
              update="qty"
              value={this.state.qty}
            />
          </Form.Field>
          <Form.Field required>
            <label>Average Price Per Oz</label>
            <Input
              label="$"
              onChange={this.updateForm}
              labelPosition="left"
              type="number"
              update="price"
              value={this.state.price}
              placeholder="Price Per Unit"
            />
          </Form.Field>
          <Form.Field required>
            <label>How Many Oz Allotted Per Person?</label>
            <Input
              type="number"
              onChange={this.updateForm}
              placeholder="Ounces Per Person"
              update="ouncesPerPerson"
              value={this.state.ouncesPerPerson}
            />
          </Form.Field>
          { this.showIngredientSummary() }
          <Input
            type="submit"
            value="Submit!"
            // onClick={this.handleFormSubmit}
            style={{ display: 'block', margin: 'auto' }}
          />
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  addSavedItemMessage: bindActionCreators(addSavedItemMessage, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditIngredient);
