/* eslint react/forbid-prop-types:0 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { formatIngredient, configureCost } from '../../utility';

class IngredientsTable extends Component {
  constructor(props) {
    super(props);

    this.renderIngredients = () => {
      const { ingredients } = this.props;
      const { ingredientsPercentages } = this.props;
      const ingredientList = ingredients.map((ingObject, i) => {
        const { ingredient } = ingObject;
        const percentValue = ingredientsPercentages[i];
        const formattedIngredient = formatIngredient(
          ingredient,
          this.props.people,
          this.props.ozPerPerson,
          percentValue,
        );
        const key = `key-${i}`;
        return this.recipeIngredientMarkup(formattedIngredient, key);
      });

      return (
        <Table columns={5}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Type</Table.HeaderCell>
              <Table.HeaderCell>Amount (Oz)</Table.HeaderCell>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Amount/Serving <span className="red">*</span></Table.HeaderCell>
              <Table.HeaderCell>Cost</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            { ingredientList }
          </Table.Body>
        </Table>
      );
    };

    this.recipeIngredientMarkup = (ingr, key) => {
      const {
        name,
        qty,
        amt,
        amtPerServing,
      } = ingr;
      const cost = configureCost(ingr, amt);

      return (
        <Table.Row id={name.replace(' ', '-')} key={key}>
          <Table.Cell>{ingr.classification}</Table.Cell>
          <Table.Cell>{`${amt} oz.`}</Table.Cell>
          <Table.Cell>{name}</Table.Cell>
          <Table.Cell>{`${amtPerServing} oz.`}</Table.Cell>
          <Table.Cell>{`$${cost} ($${ingr.price.toFixed(2)}/oz)`}</Table.Cell>
        </Table.Row>
      );
    };
  }

  render() {
    return (
      <div id="ingredientTable">
        <h3 style={{ marginTop: 15 }}>You are going to need</h3>
        { this.renderIngredients() }
      </div>
    );
  }
}

IngredientsTable.propTypes = {
  ingredients: PropTypes.array.isRequired,
  ingredientsPercentages: PropTypes.array.isRequired,
  ozPerPerson: PropTypes.string.isRequired,
  people: PropTypes.string.isRequired,
};

const mapStateToProps = state => state;

export default connect(mapStateToProps, null)(IngredientsTable);
