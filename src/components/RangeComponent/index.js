import React from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { calculateIngredientsPercentages } from '../../utility';
import './RangeComponent.css';

const { createSliderWithTooltip } = Slider;
const Range = createSliderWithTooltip(Slider.Range);

const RangeComponent = ({
  typeArray,
  rangeValues,
  setRangeValues,
  setPercentages,
}) => {
  const disabled = rangeValues.length <= 2;
  const trackStyle = typeArray.map(ing => (
    { backgroundColor: ing.color }
  ));
  return (
    <div id="range">
      <Range
        count={typeArray.length}
        defaultValue={rangeValues}
        disabled={disabled}
        onAfterChange={(e) => {
          const ingredientsPercentages = calculateIngredientsPercentages(e);
          setRangeValues(e);
          setPercentages(ingredientsPercentages);
        }}
        pushable="1"
        allowCross="false"
        trackStyle={trackStyle}
      />
      <h4 className="orientationHelp">TRY TURNING YOUR PHONE</h4>
    </div>
  );
};

export default RangeComponent;
