import React, { Component } from 'react';
import { Modal, Button, List, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import api from '../../api';
import { fetchAllRecipes } from '../../actions/recipes';
// import './AddTemplateModal.css';

class AddTemplateModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
      recipes: [],
    };

    this.handleOpen = (e) => {
      e.preventDefault();
      return this.setState({ modalOpen: true });
    };

    this.handleClose = (e) => {
      const { id } = e.target;
      this.setState({ modalOpen: false });
      return this.props.addTemplateId(id);
    };

    this.renderTemplates = () => {
      const lis = this.state.recipes.map((recipe, i) => {
        const properName = recipe.name;
        const id = recipe._id;
        const key = `item-${i}`;
        return (
          <List.Item key={key}>
            <List.Content floated="left">
              {properName}
            </List.Content>
            <List.Content floated="right">
              <List.Content floated="right">
                <Button
                  id={id}
                  onClick={this.handleClose}
                >
                  Add
                </Button>
              </List.Content>
            </List.Content>
          </List.Item>
        );
      });

      return (
        <List divided verticalAlign="middle">{ lis }</List>
      );
    };
  }
  componentDidMount() {
    api.getAllRecipes()
      .then((recipes) => {
        this.setState({ recipes: recipes.data });
      });
  }
  render() {
    return (
      <div className="addTemplateModal">
        <Modal
          trigger={
            <Button basic size="small" onClick={this.handleOpen}>
              <Icon name="add" /><Icon name="add" />Add Ingredients from Template
            </Button>
          }
          open={this.props.open}
          closeIcon
          dimmer="blurring"
          open={this.state.modalOpen}
          onClose={this.handleClose}
          style={{ marginTop: 20, margin: 'auto' }}
        >
          <Modal.Header>Add Ingredients from Template</Modal.Header>
          <Modal.Content>
            { this.renderTemplates() }
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  fetchAllRecipes: bindActionCreators(fetchAllRecipes, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTemplateModal);

// export default AddTemplateModal;
