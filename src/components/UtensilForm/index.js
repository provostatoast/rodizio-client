/* eslint class-methods-use-this:0 */
/* eslint max-len:0 */
/* eslint object-curly-newline:0 */

import React, { Component } from 'react';
import { Form, Input, Label, Loader, Dimmer } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addSavedItemMessage } from '../../reducers/messages';
import { titleCase } from '../../utility';
import defaultState from '../../defaultStates/utensil';

const translateToNumber = ['defaultServing', 'amt', 'qty', 'panAmount'];

class EditUtensil extends Component {
  constructor(props) {
    super(props);
    this.state = defaultState;

    this.loader = () => {
      if (this.state.showLoader) {
        return (
          <Dimmer active>
            <Loader size="large">Loading Utensil Data...</Loader>
          </Dimmer>
        );
      }
      return false;
    };

    this.showLoader = () => (
      this.setState({ showLoader: true })
    );

    this.clearForm = () => {
      this.setState(defaultState);
    };

    this.updateForm = (e, data) => {
      const valObj = {};
      const indexInTranslateArray = translateToNumber.indexOf(data.update);

      valObj[data.update] = indexInTranslateArray > -1
        ? Number(data.value)
        : data.value;

      this.setState(valObj);
    };

    this.showState = () => {
      console.log(this.state);
    };

    this.setUtensilToState = (u) => {
      const {
        name,
        image,
        cost,
        link,
      } = u;

      this.setState({
        name: titleCase(name),
        image,
        cost,
        link,
      }, () => {
        this.setState({ showLoader: false });
      });
    };
  }

  componentDidMount() {
    if (this.props.type === 'edit') {
      this.showLoader();
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setUtensilToState(nextProps.utensil);
  }

  render() {
    return (
      <div>
        { this.loader() }
        <Form onSubmit={this.props.handleFormSubmit}>
          <h2 style={{ textAlign: 'center' }}>Edit { this.state.name }</h2>
          <hr />
          <Form.Field required>
            <Label>Utensil Name</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Utensil Name"
              update="name"
              value={this.state.name}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Utensil Image</Label>
            <img src={this.state.image} alt="Utensil" />
            <Input
              onChange={this.updateForm}
              placeholder="Utensil Image URL"
              update="image"
              value={this.state.image}
            />
          </Form.Field>
          <Form.Field required>
            <Label>Cost for Utensil</Label>
            <Input
              label="$"
              onChange={this.updateForm}
              labelPosition="left"
              type="number"
              update="cost"
              value={this.state.cost}
              placeholder="Price Per Utensil"
            />
          </Form.Field>
          <Form.Field required>
            <Label>Utensil Link</Label>
            <Input
              onChange={this.updateForm}
              placeholder="Utensil URL"
              update="link"
              value={this.state.link}
            />
          </Form.Field>
          <Input
            type="submit"
            value="Submit!"
            // onClick={this.handleFormSubmit}
            style={{ display: 'block', margin: 'auto' }}
          />
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  addSavedItemMessage: bindActionCreators(addSavedItemMessage, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditUtensil);
