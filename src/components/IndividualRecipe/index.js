/* eslint class-methods-use-this:0 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Dimmer, Loader, Button, Modal, Form } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { fetchRecipeById, addNewRecipe } from '../../actions/recipes';
import { showModal, hideModal } from '../../actions/modal';
import { setName } from '../../actions/name';
import ConfigurationForm from '../../components/ConfigurationForm';
import RangeContainer from '../../containers/RangeContainer';
import IngredientsTable from '../../components/IngredientsTable';
import './IndividualRecipe.css';
// import SocialLoginLinks from './SocialLoginLinks';

class IndividualRecipe extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.updateName = (e) => {
      this.props.setName(e.target.value)
    };

    this.showLoader = () => {
      if (this.props.showRecipeLoader) {
        return (
          <Dimmer active>
            <Loader size="large">Loading Recipe...</Loader>
          </Dimmer>
        );
      }
      return false;
    };

    this.showRangeComponent = () => {
      if (this.state.showRange) {
        return (
          <RangeContainer
            showRange="true"
            ings={this.state.ingredients}
          />
        );
      }
      return false;
    };

    this.showSaveConfiguration = () => (
      <Button
        size="large"
        color="blue"
        onClick={this.props.showModal}
      >
        Save Configuration
      </Button>
    );

    this.handleFormSubmit = (e) => {
      e.preventDefault();
      this.props.addNewRecipe();
      this.props.hideModal();
    };

    this.removeLoadingState = () => {
      this.toggleConfigurationForm();
      this.toggleRange();
      this.toggleLoader();
    };
  }

  componentDidMount() {
    if (this.props.match.params.name) {
      this.props.fetchRecipeById(this.props.match.params.name);
    }
  }

  render() {
    return (
      <div>
        { this.showLoader() }
        <h1>{ this.props.name }</h1>
        <div className="recipeConfiguration">
          <div className="recipeConfiguration__box">
            <ConfigurationForm showConfiguration />
          </div>
          <div className="recipeConfiguration__box">
            <RangeContainer
              showRange
              ings={this.props.ingredients}
            />
          </div>
        </div>
        <div>
          { this.showSaveConfiguration() }
          <Modal
            style={{ marginTop: 0 }}
            open={this.props.modalDisplay}
            onClose={this.props.hideModal}
          >
            <Modal.Header>Give It A Name</Modal.Header>
            <Modal.Content>
              <Form>
                <Form.Field>
                  <label>Configuration Name</label>
                  <Form.Input
                    placeholder="Configuration Name"
                    onChange={this.updateName}
                    value={this.props.name}
                  />
                </Form.Field>
                <Form.Field>
                  <Button
                    color="blue"
                    onClick={this.handleFormSubmit}
                  >
                    Save Configuration
                  </Button>
                </Form.Field>
              </Form>
            </Modal.Content>
          </Modal>
        </div>
        <IngredientsTable />
      </div>
    );
  }
}

IndividualRecipe.propTypes = {
  // setIngredients: PropTypes.func.isRequired,
  fetchRecipeById: PropTypes.func.isRequired,
  addNewRecipe: PropTypes.func.isRequired,
};

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  setName: bindActionCreators(setName, dispatch),
  fetchRecipeById: bindActionCreators(fetchRecipeById, dispatch),
  addNewRecipe: bindActionCreators(addNewRecipe, dispatch),
  showModal: bindActionCreators(showModal, dispatch),
  hideModal: bindActionCreators(hideModal, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(IndividualRecipe);
