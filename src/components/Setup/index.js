import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import { Button, Divider, Transition } from 'semantic-ui-react';

import { setRangeValueArray } from '../../utility';
// import api from '../../api';
import ConfigurationForm from '../ConfigurationForm';
import AddRecipe from '../AddRecipe';
import RangeContainer from '../../containers/RangeContainer';
import { setIngredients } from '../../reducers/ingredients';
import { setRangeValues } from '../../reducers/ranges';
import { setPercentages } from '../../reducers/percentages';
import './Setup.css';

// let apiIngs = [];

class Setup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSection: 1,
      people: this.props.people,
      ozPerPerson: this.props.ozPerPerson,
    };

    this.isActive = val => (
      `transitionBlock ${(this.state.activeSection === val) ? 'show' : ''}`
    );

    this.prevButton = () => {
      const disabled = this.state.activeSection === 1;
      return (
        <Button
          size="huge"
          floated="left"
          color="blue"
          content="Go Back"
          onClick={this.nextSection}
          disabled={disabled}
        />
      );
    };

    this.nextButton = () => {
      const disabled = this.state.activeSection === 3;
      return (
        <Button
          size="huge"
          floated="right"
          color="blue"
          content="Next"
          onClick={this.nextSection}
          disabled={disabled}
        />
      );
    };

    this.totalOunces = () => {
      const { people, ozPerPerson } = this.state;
      return people * ozPerPerson;
    };

    this.prevSection = () => {
      let { activeSection } = this.state;
      this.setState({ activeSection: activeSection -= 1 }, () => console.log(this.state));
    };

    this.nextSection = () => {
      let { activeSection } = this.state;
      this.setState({ activeSection: activeSection += 1 }, () => console.log(this.state));
    };
  }

  // componentWillReceiveProps(nextProps) {
  //   this.setState({
  //     ingredients: nextProps.ingredients,
  //     people: nextProps.people,
  //     ozPerPerson: nextProps.ozPerPerson,
  //   });
  // }

  render() {
    return (
      <div>
        <div className="setupGroup">
          <div className={this.isActive(1)}>
            <h1 className="center">How many people are you serving?</h1>
            <h3 className="center">And How Much Does Each Person Get?</h3>
            <ConfigurationForm showConfiguration="true" />
          </div>
          <div className={this.isActive(2)}>
            <h1 className="center">That&apos;s { this.totalOunces() } Total Ounces To Work With.</h1>
            <h3 className="center">What should we do with it?</h3>
            <AddRecipe setup="true" />
          </div>
          <Transition
            visible={this.state.activeSection === 3}
            duration={200}
            animation="fade"
            unmountOnHide={true}
          >
            <div className="transitionBlock">
              <h1>Set Your Levels!</h1>
              <RangeContainer
                showRange
                ings={this.props.ingredients}
              />
              <Button
                color="green"
                size="huge"
                as={Link}
                to="/configuration/"
              >
                Show Configuration
              </Button>
            </div>
          </Transition>
        </div>
        <div>
          { this.prevButton() }
          { this.nextButton() }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  setRangeValues: bindActionCreators(setRangeValues, dispatch),
  setPercentages: bindActionCreators(setPercentages, dispatch),
  setIngredients: bindActionCreators(setIngredients, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Setup);
