import React from 'react';
import { formatIngredient, getRandomColor } from '../../utility';

const IngredientLegend = ({
  percentages,
  ingredients,
  people,
  ozPerPerson,
}) => {
  const legendMarkup = ingredients.map((ing, i) => {
    const { ingredient } = ing;
    const percentValue = percentages[i];
    console.log(percentValue);
    const formattedIngredient = formatIngredient(
      ingredient,
      people,
      ozPerPerson,
      percentValue,
    );
    console.log(formattedIngredient);
    return (
      <div key={getRandomColor()} className="legendItem">
        <div className="legendItem__color" style={{ backgroundColor: ing.color }} />
        <div className="legendItem__name">{`${ing.ingredient.name} (${formattedIngredient.amtPerServing} oz./Person)`}</div>
      </div>
    );
  });

  return legendMarkup;
};

export default IngredientLegend;
