import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Dimmer, List, Button, Loader } from 'semantic-ui-react';
import panConfigurations from '../../panConfigurations';

import logo from './logo.svg';
import './App.css';
import api from '../../api';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recipes: [],
      showLoader: true,
    };
  }

  componentDidMount() {
    api.getAllRecipes()
      .then((recipes) => {
        // console.log(`Recipes: ${JSON.stringify(recipes)}`);
        this.setState({ recipes: recipes.data });
        this.setState({ showLoader: false });
      });
  }

  showLoader() {
    if (this.state.showLoader) {
      return (
        <Dimmer active>
          <Loader size="large">Loading Recipes...</Loader>
        </Dimmer>
      );
    }
    return false;
  }

  renderRecipes() {
    const lis = this.state.recipes.map((recipe, i) => {
      const properName = recipe.name;
      const id = recipe._id;
      const key = `item-${i}`;
      return (
        <List.Item key={key}>
          <List.Content floated="left">
            <Link className="listItemName" to={`/recipes/recipe/${id}`}>{properName}</Link>
          </List.Content>
          <List.Content floated="right">
            <List.Content floated="right">
              <Button
                as={Link}
                to={`/recipes/edit/${id}`}
              >
                Edit
              </Button>
            </List.Content>
          </List.Content>
        </List.Item>
      );
    });

    return (
      <List divided verticalAlign="middle">{ lis }</List>
    );
  }

  render() {
    panConfigurations();
    return (
      <div className="App">
        { this.showLoader() }
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">What are you going to make?</h1>
        </header>
        <div className="container">
          <div className="contentHeader">
            <h2>Recipes</h2>
            <Button className="contentHeader__button" as={Link} to="/recipes/new">Create New</Button>
          </div>
          { this.renderRecipes() }
        </div>
      </div>
    );
  }
}

export default Home;
