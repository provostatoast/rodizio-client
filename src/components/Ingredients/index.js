import React, { Component } from 'react';
import { connect } from 'react-redux';
import { List, Button, Dimmer, Loader, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import AddIngredientModal from '../Modal';
import { fetchAllIngredients, deleteIngredient } from '../../actions/ingredients';
import { showModal, hideModal } from '../../actions/modal';
import api from '../../api';
import './ingredients.css';
// import SocialLoginLinks from './SocialLoginLinks';

class Ingredients extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   showModal: false,
    //   showLoader: true,
    // };

    this.removeIngredient = (e) => {
      e.preventDefault();
      const { id } = e.target;
      this.props.deleteIngredient(id);
    };

    this.showModalToggle = () => {
      this.props.showModal();
    };

    this.closeModalAndUpdateIngredients = () => {
      this.props.hideModal();
      this.getUpdatedIngredientsList();
    };

    this.showLoader = () => {
      if (this.props.showIngredientsLoader) {
        return (
          <Dimmer active>
            <Loader size="large">Loading Ingredients...</Loader>
          </Dimmer>
        );
      }
      return false;
    };

    this.getUpdatedIngredientsList = () => {
      this.props.fetchAllIngredients();
    };
  }

  componentDidMount() {
    this.getUpdatedIngredientsList();
  }

  render() {
    return (
      <div>
        { this.showLoader() }
        <h2>All Ingredients</h2>
        <List divided verticalAlign="middle">
          {
            this.props.ingredients.map((ingredient, i) => {
              const properName = ingredient.name;
              const key = `item-${i}`;
              return (
                <List.Item>
                  <List.Content floated="left">
                    <Link className="listItemName" to={`/ingredients/edit/${ingredient._id}`}>{properName}</Link>
                  </List.Content>
                  <List.Content floated="right">
                    <List.Content floated="right">
                      <Button color="red" id={ingredient._id} onClick={this.removeIngredient}>
                        <Icon name="close" /> Delete
                      </Button>
                    </List.Content>
                    <List.Content floated="right">
                      <Button
                        as={Link}
                        to={`/ingredients/edit/${ingredient._id}`}
                      >
                        <Icon name="edit" /> Edit
                      </Button>
                    </List.Content>
                  </List.Content>
                </List.Item>
              );
            })
          }
        </List>
        <br /><br />
        <AddIngredientModal updateItems={this.getUpdatedIngredientsList} />
      </div>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => ({
  fetchAllIngredients: bindActionCreators(fetchAllIngredients, dispatch),
  deleteIngredient: bindActionCreators(deleteIngredient, dispatch),
  showModal: bindActionCreators(showModal, dispatch),
  hideModal: bindActionCreators(hideModal, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Ingredients);
