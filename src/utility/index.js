/* eslint no-plusplus:0 */

const capitalizeFirstLetter = () => 'Hello';

export const titleCase = (str) => {
  const lower = str.toLowerCase();
  return lower.replace(/(^|\s)[a-z]/g, f => f.toUpperCase());
};

export const getRandomColor = () => {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i += 1) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

export const ozToLbs = oz => oz / 16;

export const qtyObject = (qty, amt, ratio) => {
  const obj = {
    weight: 0,
    unit: '',
  };

  const qtyArray = amt.split('-');
  if (ratio) {
    obj.weight = Math.ceil(Number(qtyArray[0]) * qty * ratio);
  } else {
    obj.weight = Number(qtyArray[0]);
  }

  obj.unit = qtyArray[1];

  return `${obj.weight}-${obj.unit}`;
};

export const calculateIngredientAmount = (percent, totalOz) => (
  Math.round((percent * 0.01) * totalOz)
);

export const calculateIngredientsPercentages = (rangeValues) => {
  const ingredientsLength = rangeValues.length;
  const ingredientsPercentages = [];

  for (let i = 1; i < ingredientsLength; i += 1) {
    const value = rangeValues[i] - rangeValues[i - 1];
    ingredientsPercentages.push(value);
  }

  return ingredientsPercentages;
};

export const assignColorsToIngredients = (ings) => {
  const ingredients = ings.map((ing) => {
    const ingCopy = ing;
    ingCopy.color = getRandomColor();
    return ingCopy;
  });
  return ingredients;
};

export const setRangeValueArray = (ingredients) => {
  const ingredientsLength = ingredients.length;
  const ingredientsDiv = Math.floor(100 / ingredientsLength);
  const rangeValues = [0];
  for (let i = 1; i < ingredientsLength; i += 1) {
    rangeValues[i] = ingredientsDiv * i;
  }

  rangeValues[ingredientsLength] = 100;

  const ingredientsPercentages = calculateIngredientsPercentages(rangeValues);

  return {
    rangeValues,
    ingredientsPercentages,
  };
};

export const weightConversion = (unit, amountInOunces, itemInOunces) => {
  const conversionNumber = {
    lb: Math.ceil(ozToLbs(amountInOunces), 1),
    oz: amountInOunces,
    unit: Math.round(amountInOunces / itemInOunces, 1),
  };

  const conversionString = {
    lb: `${Math.ceil(ozToLbs(amountInOunces), 1)} lbs.`,
    oz: `${amountInOunces} oz.`,
    unit: Math.round(amountInOunces / itemInOunces, 1),
  };
  return {
    conversionNumber: conversionNumber[unit],
    conversionString: conversionString[unit],
  };
};

export const configureCost = (ingredient, amt) => {
  const { price } = ingredient;
  const totalPrice = price * amt;

  return totalPrice.toFixed(2);
};

export const formatIngredient = (
  ingredient,
  servings,
  ozPerPerson,
  percentage,
) => {
  // const { amt } = ingredient;
  const totalOz = ozPerPerson * servings;
  const amt = calculateIngredientAmount(percentage, totalOz);
  // const qty = weightConversion(weightUnit, amt);
  const ratioRoundedUp = 1;

  const formattedIngredient = {
    amt,
    panAmount: ratioRoundedUp,
    qty: amt,
    amtPerServing: ((percentage * 0.01) * ozPerPerson).toFixed(1),
  };

  return Object.assign({}, ingredient, formattedIngredient);
};

export const extend = (obj3, obj1, obj2) => {
  const o3 = obj3;

  Object.keys(obj1).forEach((k) => {
    o3[k] = obj1[k];
  });

  Object.keys(obj2).forEach((k) => {
    if (o3.hasOwnProperty(k)) {
      o3[k] += obj2[k];
    } else {
      o3[k] = obj2[k];
    }
  });

  return o3;
};

export const allPossibleCases = (arr) => {
  if (arr.length === 1) {
    return arr[0];
  }
  const result = [];
  const allCasesOfRest = allPossibleCases(arr.slice(1));
  for (let i = 0; i < allCasesOfRest.length; i++) {
    for (let j = 0; j < arr[0].length; j++) {
      const res = extend({}, arr[0][j], allCasesOfRest[i]);
      result.push(res);
    }
  }
  return result;
};

export const sortObject = (obj) => {
  const depths = [];
  const sortedObj = {};
  Object.keys(obj).map(k => depths.push(k));

  depths.sort().reverse();
  depths.forEach((d) => {
    sortedObj[d] = obj[d];
  });

  return sortedObj;
};

export const configFromDups = (dupsObj) => {
  const config = [];

  Object.keys(dupsObj).forEach((key) => {
    for (let i = 0; i < dupsObj[key]; i++) {
      config.push(key);
    }
  });

  return config;
};

export const countDups = (arr) => {
  const counts = {};
  arr.forEach((x) => {
    counts[x] = (counts[x] || 0) + 1;
    return true;
  });

  return counts;
};

export const flattenConfigurations = (configs) => {
  const flattened = [];

  Object.keys(configs).forEach((key) => {
    const configArray = configs[key].map(c => c.name);

    if (configArray.length > 0) flattened.push(configArray);
  });

  return flattened;
};

export const removeAllItemsFromArray = (arr1, arr2) => {
  const result = arr2;
  for (let i = 0; i < arr1.length; i++) {
    if (arr2.indexOf(arr1[i]) > -1) {
      result.splice(arr2.indexOf(arr1[i]), 1);
    }
  }

  return result;
};

export const getDupsFromObject = (obj) => {
  const dupObj = {};

  Object.keys(obj).forEach((k) => {
    const configDups = countDups(obj[k]);
    dupObj[k] = configDups;
  });

  return dupObj;
};

export default capitalizeFirstLetter;
