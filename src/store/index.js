/* eslint no-undef:0 */
/* eslint no-underscore-dangle:0 */

import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import { routerReducer } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';
import { authReducer } from '../reducers/auth';
import peopleReducer from '../reducers/people';
import rangeReducer from '../reducers/ranges';
import percentageReducer from '../reducers/percentages';
import ozPerPersonReducer from '../reducers/ozPerPerson';
import ingredientsReducer from '../reducers/ingredients';
import nameReducer from '../reducers/name';
import ingredientsLoader from '../reducers/ingredientsLoader';
import recipesLoader from '../reducers/recipesLoader';
import modalReducer from '../reducers/modal';

const history = createBrowserHistory();
const routing = routerMiddleware(history);

const rootReducer = combineReducers({
  routing: routerReducer,
  authReducer,
  people: peopleReducer,
  ozPerPerson: ozPerPersonReducer,
  rangeValues: rangeReducer,
  ingredientsPercentages: percentageReducer,
  ingredients: ingredientsReducer,
  name: nameReducer,
  showIngredientsLoader: ingredientsLoader,
  showRecipeLoader: recipesLoader,
  modalDisplay: modalReducer,
});

function configureStoreProd(preloadedState) {
  const middlewares = [routing, thunkMiddleware];

  return createStore(
    connectRouter(history)(rootReducer),
    preloadedState,
    compose(applyMiddleware(...middlewares)),
  );
}

function configureStoreDev(preloadedState) {
  const middlewares = [routing, thunkMiddleware];
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  return createStore(
    connectRouter(history)(rootReducer),
    preloadedState,
    composeEnhancers(applyMiddleware(...middlewares)),
  );
}

const configureStore = process.env.NODE_ENV === 'production' ? configureStoreProd : configureStoreDev;

export default configureStore;
