import axios from 'axios';
import { getToken } from './helper';

const SERVER_URL = 'https://rodizio-revolution-be.herokuapp.com';

// auth
const register = userInfo => axios.post(`${SERVER_URL}/register`, userInfo);
const login = userInfo => axios.post(`${SERVER_URL}/login`, userInfo);

// recipes
const getAllRecipes = () => axios.get(`${SERVER_URL}/recipes/all`);
const getRecipeByName = name => axios.get(`${SERVER_URL}/recipes/name/${name}`);
const getRecipeById = id => axios.get(`${SERVER_URL}/recipes/id/${id}`);
const updateRecipeById = payload => axios.post(`${SERVER_URL}/recipes/edit/${payload._id}`, payload);
const addNewRecipe = payload => axios.post(`${SERVER_URL}/recipes/new`, payload);
const deleteRecipe = id => axios.get(`${SERVER_URL}/recipes/delete/${id}`);

// ingredients
const getAllIngredients = () => axios.get(`${SERVER_URL}/ingredients/all`);
const getIngredientById = id => axios.get(`${SERVER_URL}/ingredients/${id}`);
const getMultipleIngredientsById = payload => axios.get(`${SERVER_URL}/ingredients/multiple`, payload);
const addNewIngredient = payload => axios.post(`${SERVER_URL}/ingredients/new`, payload);
const updateIngredient = payload => axios.post(`${SERVER_URL}/ingredients/edit/${payload._id}`, payload);
const deleteIngredient = id => axios.get(`${SERVER_URL}/ingredients/delete/${id}`);

const getSecret = uid =>
  axios.get(`${SERVER_URL}/secret/${uid}`, {
    headers: { authorization: `Bearer ${getToken()}` },
  });

const api = {
  register,
  login,
  getAllRecipes,
  addNewRecipe,
  getSecret,
  getRecipeByName,
  getRecipeById,
  updateRecipeById,
  deleteRecipe,
  getAllIngredients,
  getIngredientById,
  getMultipleIngredientsById,
  addNewIngredient,
  updateIngredient,
  deleteIngredient,
};

export default api;
